#include "lie/se3.hpp"

#include "gtest/gtest.h"

#define ASSERT_MATRICES_APPROX_EQ(M_actual, M_expected) \
  ASSERT_TRUE(M_actual.isApprox(M_expected, 1e-6)) << "  Actual:\n" << M_actual << "\nExpected:\n" << M_expected

TEST(SE3Test, Test1)
{
  double PI = 3.14159265359;
  Lie::SE3 H(1.0, 2.0, 3.0, 0.0, 0.0, PI/2);

  Eigen::Matrix4d H_true;
  H_true <<
      0, -1, 0, 1, 
      1, 0, 0, 2, 
      0, 0, 1, 3,
      0, 0, 0, 1;

  ASSERT_MATRICES_APPROX_EQ(H(), H_true);
}
TEST(SE3Test, Test2)
{
  double PI = 3.14159265359;
  Lie::SO3 R(0.0, 0.0, PI/2);
  Eigen::Vector3d t;
  t << 1.0, 2.0, 3.0;
  
  Lie::SE3 H(R, t);

  Eigen::Matrix4d H_true;
  H_true <<
      0, -1, 0, 1, 
      1, 0, 0, 2, 
      0, 0, 1, 3,
      0, 0, 0, 1;

  ASSERT_MATRICES_APPROX_EQ(H(), H_true);
}

TEST(SE3MultTest, Test1)
{
  Lie::SE3 H1(1, 2, 0, 0, 0, 0);
  Lie::SE3 H2(1, 2, 3, 0, 0, 0);

  Eigen::Matrix4d H_true;
  H_true <<
      1, 0, 0, 2, 
      0, 1, 0, 4, 
      0, 0, 1, 3,
      0, 0, 0, 1;

  auto H = H1*H2;

  ASSERT_MATRICES_APPROX_EQ(H(), H_true);
}

TEST(SE3InvTest, Test1)
{
  Lie::SE3 H1(1, 2, 3, 4, 5, 6);

  Eigen::Matrix4d H_true;
  H_true <<
      1, 0, 0, 0, 
      0, 1, 0, 0, 
      0, 0, 1, 0,
      0, 0, 0, 1;

  auto H = H1.inverse()*H1;

  ASSERT_MATRICES_APPROX_EQ(H(), H_true);
}

TEST(ExpTest, Test1)
{
  double PI = 3.14159265359;
  Lie::Vector6d xi_vec;
  xi_vec << 1, 2, 3, PI/2, PI/3, PI/4;

  Lie::SE3::TangentVector xi(xi_vec);

  Eigen::Matrix4d H_true;
  H_true <<
      0.403123052181182,   0.231225946473940,   0.885452633672383,   2.260167925409578,
      0.914777793338191,  -0.074378506073872,  -0.397050911911218,   0.354269925210058,
      -0.025949828813285,   0.970052781817283,  -0.241504051463141,   2.673970915567433,
      0,                   0,                   0,   1.000000000000000;

  auto H = Lie::SE3::Exp(xi);

  ASSERT_MATRICES_APPROX_EQ(H(), H_true);
}

TEST(ExpLogTest, Test1)
{
  Lie::SE3 H(1, 2, 3, 4, 5, 6);

  Lie::SE3 H_prime = Lie::SE3::Exp( Lie::SE3::Log( H ) );

  ASSERT_MATRICES_APPROX_EQ(H_prime(), H());
}

TEST(LogExpTest, Test1)
{
  double PI = 3.14159265359;
  
  Lie::Vector6d xi_vec;
  xi_vec << 20, 1, -10, PI/2, PI/3, PI/4;

  Lie::SE3::TangentVector xi(xi_vec);

  Lie::SE3::TangentVector xi_prime = Lie::SE3::Log(Lie::SE3::Exp(xi));

  ASSERT_MATRICES_APPROX_EQ(xi_prime, xi_vec);
}

TEST(AdjointTest, Test1)
{
  Lie::Vector6d xi_vec;
  xi_vec << 1, 2, 3, 1, 2, 3;

  Lie::SE3::TangentVector xi(xi_vec);
  Lie::SE3 T = Lie::SE3::Exp(xi);

  auto Adj = Lie::SE3::Ad(T);

  Lie::Matrix6dRow Adj_true;
  Adj_true <<
      -0.694920557641312,   0.713520990527788,   0.089292858861912,     1.961977253859538,   2.174054531785847,  -2.103362105810411, 
      -0.192006972791999,  -0.303785044339471,   0.933192353823647,    -2.777739840665705,   1.509213272199645,  -0.080228901244528,
      0.692978167741770,    0.631349699383718,   0.348107477830265,     1.197834142490624,  -1.730827025395046,   0.754606636099822,
      0,                   0,                    0,                    -0.694920557641312,   0.713520990527788,   0.089292858861912,
      0,                   0,                    0,                    -0.192006972791999,  -0.303785044339471,   0.933192353823647,
      0,                   0,                    0,                     0.692978167741770,   0.631349699383718,   0.348107477830265;

  ASSERT_MATRICES_APPROX_EQ(Adj, Adj_true);
}
