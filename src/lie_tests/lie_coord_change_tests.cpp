#include "lie/coord_change.hpp"
#include "lie/se3.hpp"


#include "gtest/gtest.h"

#define ASSERT_MATRICES_APPROX_EQ(M_actual, M_expected) \
  ASSERT_TRUE(M_actual.isApprox(M_expected, 1e-6)) << "  Actual:\n" << M_actual << "\nExpected:\n" << M_expected

TEST(LogTupleTest, TestSE3)
{
  double PI = 3.14159265359;
  //  Lie::SE3 H1(0.0, 0.0, 0.0, 0.0, 0.0, PI/2);
  Lie::SE3 H1(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

  auto objects = std::make_tuple(H1);

  auto result = Lie::log_tuple(objects);
  ASSERT_MATRICES_APPROX_EQ(result, Lie::SE3::Log(H1));
}

TEST(LogTupleTest, TestTwoSE3)
{
  double PI = 3.14159265359;
  Lie::SE3 H1(0.0, 0.0, 0.0, 0.0, 0.0, PI/2);
  Lie::SE3 H2(1.0, 0.0, 0.0, 0.0, 0.0, 0.0);

  auto objects = std::make_tuple(H1, H2);

  auto result = Lie::log_tuple(objects);
  ASSERT_MATRICES_APPROX_EQ(result.head(6), Lie::SE3::Log(H1));
  ASSERT_MATRICES_APPROX_EQ(result.tail(6), Lie::SE3::Log(H2));
}

TEST(LogTupleTest, TestTwoSO3)
{
  Lie::SO3 R1(1.0, 2.0, 3.0);
  Lie::SO3 R2(7.0, 8.0, 9.0);

  auto objects = std::make_tuple(R1, R2);

  auto result = Lie::log_tuple(objects);
  ASSERT_MATRICES_APPROX_EQ(result.head(3), Lie::SO3::Log(R1));
  ASSERT_MATRICES_APPROX_EQ(result.tail(3), Lie::SO3::Log(R2));
}

Lie::SE3 xyz_to_SE3(const Eigen::VectorXd& xyz)
{
  Lie::SE3 H(Lie::SO3(), xyz);
  return H;
}

void death_test1()
{
  double PI = 3.14159265359;
  //  Lie::SE3 H1(0.0, 0.0, 0.0, 0.0, 0.0, PI/2);

  
  Eigen::VectorXd xyz(3);
  xyz << 1, 2, 3;

  Eigen::MatrixXd cov = Eigen::Matrix3d::Identity();

  std::vector<Eigen::VectorXd> means = {xyz};

  std::function< Lie::SE3(const Eigen::VectorXd&) > conv_function = xyz_to_SE3;
  Lie::convert_uncert_rep_from_coord_to_lie_algebra<Lie::SE3, 2>(
      xyz,
      cov,
      conv_function);
}

TEST(CoordChangeDeathTest, Test1)
{
  ASSERT_DEATH(death_test1(), "");
}



TEST(CoordChangeTest, TestSE3Translation)
{
  double PI = 3.14159265359;
  //  Lie::SE3 H1(0.0, 0.0, 0.0, 0.0, 0.0, PI/2);

  
  Eigen::VectorXd xyz(3);
  xyz << 1, 2, 3;

  Eigen::MatrixXd cov = Eigen::Matrix3d::Identity();

  std::vector<Eigen::VectorXd> means = {xyz};

  std::function< Lie::SE3(const Eigen::VectorXd&) > conv_function = xyz_to_SE3;
  auto result =
      Lie::convert_uncert_rep_from_coord_to_lie_algebra<Lie::SE3, 1>(
          xyz,
          cov,
          conv_function);

  auto H = std::get<0>(result);
  std::cout << H.mu() << "\n";
  std::cout << H.marginal_cov() << "\n";
}

Lie::SE3 xyzrph_to_SE3(const Eigen::VectorXd& xyzrph)
{
  Lie::SE3 H(xyzrph(0),
             xyzrph(1),
             xyzrph(2),
             xyzrph(3),
             xyzrph(4),
             xyzrph(5));
  return H;
}

TEST(CoordChangeTest, TestSE3Full)
{
  double PI = 3.14159265359;
  //  Lie::SE3 H1(0.0, 0.0, 0.0, 0.0, 0.0, PI/2);

  
  Eigen::VectorXd xyzrph(6);
  xyzrph << 3, 3, 0, 0.0, 0.0, PI/4.0;

  Eigen::VectorXd diag(6);
  diag << 0.01, 0.01, 0.00001, 0.00001, 0.00001, 0.03;
  Eigen::MatrixXd cov = diag.asDiagonal();

  std::function< Lie::SE3(const Eigen::VectorXd&) > conv_function = xyzrph_to_SE3;
  auto result =
      Lie::convert_uncert_rep_from_coord_to_lie_algebra<Lie::SE3, 1>(
          xyzrph,
          cov,
          conv_function);

  auto H = std::get<0>(result);
  std::cout << H.mu() << "\n";
  std::cout << H.marginal_cov() << "\n";
}

