#include "lie/ut.hpp"

#include "gtest/gtest.h"

#include "mr/ut.hpp"
#include "mr/stat.hpp"

#define ASSERT_MATRICES_APPROX_EQ(M_actual, M_expected) \
  ASSERT_TRUE(M_actual.isApprox(M_expected, 1e-6)) << "  Actual:\n" << M_actual << "\nExpected:\n" << M_expected

TEST(UTSigmaPointTest, Test1)
{
  Eigen::VectorXd mu(6);
  mu << 1, 2, 3, 4, 5, 6;
  Eigen::MatrixXd cov = MR::STAT::rand_cov(6);

  UT::UnscentedTransformInput ut_input(mu, cov);

  MR::UT::ut_t mr_ut_input;
  MR::UT::unscented_transform(&mr_ut_input, mu, cov);

  ASSERT_MATRICES_APPROX_EQ(ut_input.mean_weights(), mr_ut_input.mean_weight);
  ASSERT_MATRICES_APPROX_EQ(ut_input.cov_weights(), mr_ut_input.cov_weight);
  ASSERT_MATRICES_APPROX_EQ(ut_input.sigma_points(), mr_ut_input.sigma_points);  
}


TEST(UTSigmaPointTest, Test2)
{
  Eigen::VectorXd mu(6);
  mu << 1, 2, 5, 6, 2, 14;
  Eigen::MatrixXd cov = MR::STAT::rand_cov(6);

  UT::UnscentedTransformInput ut_input(mu, cov);

  MR::UT::ut_t mr_ut_input;
  MR::UT::unscented_transform(&mr_ut_input, mu, cov);

  ASSERT_MATRICES_APPROX_EQ(ut_input.mean_weights(), mr_ut_input.mean_weight);
  ASSERT_MATRICES_APPROX_EQ(ut_input.cov_weights(), mr_ut_input.cov_weight);
  ASSERT_MATRICES_APPROX_EQ(ut_input.sigma_points(), mr_ut_input.sigma_points);  
}

Eigen::VectorXd elementwise_sin(const Eigen::VectorXd& input)
{
  Eigen::VectorXd output(input.rows());
  for(int i=0;i<input.rows();i++)
  {
    output(i) = sin(input(i));
  }
  return output;
}

Eigen::VectorXd elementwise_sin_c(const Eigen::VectorXd& input,  void* ptr)
{
  return elementwise_sin(input);
}

TEST(UTFullTest, Test1)
{
  Eigen::VectorXd mu(6);
  mu << 1, 2, 3, 4, 5, 6;
  Eigen::MatrixXd cov = MR::STAT::rand_cov(6);

  // Test the local c++ library
  UT::UnscentedTransformInput ut_input(mu, cov);
  auto result = UT::evaluate_unscented_transform(elementwise_sin, ut_input);

  MR::UT::ut_t mr_ut_input;
  MR::UT::unscented_transform(&mr_ut_input, mu, cov);
  MR::UT::ut_return_t mr_ut_output;
  MR::UT::unscented_func(&mr_ut_output, elementwise_sin_c, &mr_ut_input, NULL);
  
  ASSERT_MATRICES_APPROX_EQ(result.mu, mr_ut_output.mu_prime);
  ASSERT_MATRICES_APPROX_EQ(result.Sigma, mr_ut_output.sigma_prime);
}

