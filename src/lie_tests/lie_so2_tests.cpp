#include "lie/so2.hpp"

#include "gtest/gtest.h"

#define ASSERT_MATRICES_APPROX_EQ(M_actual, M_expected) \
  ASSERT_TRUE(M_actual.isApprox(M_expected, 1e-6)) << "  Actual:\n" << M_actual << "\nExpected:\n" << M_expected

TEST(SO2ConstructionTest, CopyTest)
{
  double PI = 3.14159265359;
  Lie::SO2 R(PI/2);

  Eigen::Matrix2d R_true;
  R_true <<
      0, -1,
      1, 0;

  Lie::SO2 R2 = R;
  Lie::SO2 R3(R);
  
  ASSERT_MATRICES_APPROX_EQ(R2(), R_true);
  ASSERT_MATRICES_APPROX_EQ(R3(), R_true);
}

TEST(SO2ConstructionTest, MatrixConstuctorTest)
{
  Eigen::Matrix2d R_true;
  R_true <<
      0, -1,
      1, 0;

  Lie::SO2 R(R_true);
  
  ASSERT_MATRICES_APPROX_EQ(R(), R_true);
}

TEST(SO2MultTest, Test1)
{
  double PI = 3.14159265359;
  Lie::SO2 R1(PI/2);
  Lie::SO2 R2(PI/2);
  Lie::SO2 R3(PI/2);
  Lie::SO2 R4(PI/2);

  Eigen::Matrix2d R_true;
  R_true <<
      1, 0,
      0, 1;

  auto R = R1*R2*R3*R4;

  ASSERT_MATRICES_APPROX_EQ(R(), R_true);
}

TEST(SO2MultTest, Test2)
{
  double PI = 3.14159265359;
  Lie::SO2 R1(PI/2);
  Eigen::Vector2d vec;
  vec << 1, 0;

  Eigen::Vector2d V_true;
  V_true << 0, 1;


  auto V = R1*vec;

  ASSERT_MATRICES_APPROX_EQ(V, V_true);
}

TEST(SO2ComposeTest, Test1)
{
  double PI = 3.14159265359;
  Lie::SO2 R1(PI/2);
  Lie::SO2 R2(PI/2);
  Lie::SO2 R3(PI/2);
  Lie::SO2 R4(PI/2);

  Eigen::Matrix2d R_true;
  R_true <<
      1, 0,
      0, 1;

  auto R_Compose1 = R1.compose(R2).compose(R3).compose(R4);
  auto R_Compose2 = compose(compose(R1, R2), compose(R3, R4));

  ASSERT_MATRICES_APPROX_EQ(R_Compose1(), R_true);
  ASSERT_MATRICES_APPROX_EQ(R_Compose2(), R_true);
}

TEST(SO2BetweenTest, Test1)
{
  double PI = 3.14159265359;
  Lie::SO2 R1(0);
  Lie::SO2 R2(PI/2);
  Lie::SO2 R3(PI);

  Lie::SO2 R4(-PI);


  auto R_Between1a = R1.between(R3);
  ASSERT_MATRICES_APPROX_EQ(R_Between1a(), R3());
  auto R_Between1b = between(R1, R3);
  ASSERT_MATRICES_APPROX_EQ(R_Between1b(), R3());
  
  auto R_Between2a = R1.between(R3);
  ASSERT_MATRICES_APPROX_EQ(R_Between2a(), R3());
  auto R_Between2b = between(R1, R3);
  ASSERT_MATRICES_APPROX_EQ(R_Between2b(), R3());  

  auto R_Between3a = R2.between(R3);
  ASSERT_MATRICES_APPROX_EQ(R_Between3a(), R2());
  auto R_Between3b = between(R2, R3);
  ASSERT_MATRICES_APPROX_EQ(R_Between3b(), R2());  

  auto R_Between4a = R3.between(R1);
  ASSERT_MATRICES_APPROX_EQ(R_Between4a(), R4());
  auto R_Between4b = between(R3, R1);
  ASSERT_MATRICES_APPROX_EQ(R_Between4b(), R4());  
}



TEST(SO2InvTest, Test1)
{
  Lie::SO2 R1(4);

  Eigen::Matrix2d R_true;
  R_true <<
      1, 0,
      0, 1;

  auto R = R1.inverse()*R1;

  ASSERT_MATRICES_APPROX_EQ(R(), R_true);
}

TEST(ExpTest, Test1)
{
  double PI = 3.14159265359;
  Eigen::Vector1d w;
  w << PI/2.0;

  Lie::SO2::TangentVector phi(w);

  Lie::SO2 R = Lie::SO2::Exp(phi);

  Eigen::Matrix2d R_true;
  R_true <<
      0, -1,
      1, 0;

  ASSERT_MATRICES_APPROX_EQ(R(), R_true);
}


TEST(ExpTest, ZeroTest)
{
  Eigen::Vector1d w;
  w << 0;

  Lie::SO2::TangentVector phi(w);

  Lie::SO2 R = Lie::SO2::Exp(phi);

  Eigen::Matrix2d R_true;
  R_true <<
      1, 0,
      0, 1;

  ASSERT_MATRICES_APPROX_EQ(R(), R_true);
}

TEST(LogTest, Test1)
{
  double PI = 3.14159265359;
  Eigen::Vector1d w_true;
  w_true << PI/2.;

  Eigen::Matrix2d R;
  R <<
      0, -1,
      1, 0;
  Lie::SO2 Rot(R);
  
  Lie::SO2::TangentVector w = Lie::SO2::Log(Rot);

  ASSERT_MATRICES_APPROX_EQ(w, w_true);
}

TEST(LogTest, ZeroTest)
{
  double PI = 3.14159265359;
  Eigen::Vector1d w_true;
  w_true << 0;

  Eigen::Matrix2d R;
  R <<
      1, 0,
      0, 1;

  Lie::SO2 Rot(R);
  
  Lie::SO2::TangentVector w = Lie::SO2::Log(Rot);

  ASSERT_MATRICES_APPROX_EQ(w, w_true);
}



TEST(LogExpTest, Test1)
{
  double PI = 3.14159265359;
  Eigen::Vector1d w_true;
  w_true << PI/6.5;

  Lie::SO2::TangentVector w(w_true);
  Lie::SO2::TangentVector w_prime = Lie::SO2::Log(Lie::SO2::Exp(w));

  ASSERT_MATRICES_APPROX_EQ(w_prime, w_true);

}

TEST(ExpLogTest, Test1)
{
  double PI = 3.14159265359;

  Eigen::Matrix2d R_true;
  R_true <<
      0, -1,
      1, 0;
  
  Lie::SO2 Rot(R_true);
  Lie::SO2 Rot_prime = Lie::SO2::Exp(Lie::SO2::Log(Rot));

  ASSERT_MATRICES_APPROX_EQ(Rot_prime(), R_true);

}
