#include "lie/lie.hpp"

#include "gtest/gtest.h"

#define ASSERT_MATRICES_APPROX_EQ(M_actual, M_expected) \
  ASSERT_TRUE(M_actual.isApprox(M_expected, 1e-6)) << "  Actual:\n" << M_actual << "\nExpected:\n" << M_expected

class UncertainStateTest : public ::testing::Test {
 public:
  class GroupOne : public Lie::LieGroupBase
  {
   public:
    GroupOne() : Lie::LieGroupBase(1){}
  };  
  class GroupTwo : public Lie::LieGroupBase
  {
   public:
    GroupTwo() : Lie::LieGroupBase(2){}
  };
  class GroupThree : public Lie::LieGroupBase
  {
   public:
    GroupThree() : Lie::LieGroupBase(3){}
  };
};

TEST_F(UncertainStateTest, ConstructionTest1)
{
  GroupOne a;
  GroupOne b;
  GroupOne c;

  Eigen::MatrixXd cov(3,3);
  cov <<
      1, 2, 3,
      2, 4, 5,
      3, 5, 6;

  Lie::UncertainState<GroupOne, GroupOne, GroupOne> state(std::make_tuple(a, b, c), cov);
  ASSERT_EQ(3, state.get_num_objects());
}

TEST_F(UncertainStateTest, ConstructionTest2)
{
  GroupOne a;
  GroupTwo b;

  Eigen::MatrixXd cov(3,3);
  cov <<
      1, 2, 3,
      2, 4, 5,
      3, 5, 6;

  Lie::UncertainState<GroupOne, GroupTwo> state(std::make_tuple(a, b), cov);
  ASSERT_EQ(2, state.get_num_objects());
}

void const_death_test1(UncertainStateTest::GroupOne a,
                       UncertainStateTest::GroupTwo b,
                       Eigen::MatrixXd& cov)
{
  Lie::UncertainState<UncertainStateTest::GroupOne,
                      UncertainStateTest::GroupTwo> state(std::make_tuple(a, b), cov);
}

TEST_F(UncertainStateTest, ConstructionDeathTest1)
{
  GroupOne a;
  GroupTwo b;

  Eigen::MatrixXd cov(2,2);
  cov <<
      1, 2,
      2, 4;

  ASSERT_DEATH(const_death_test1(a, b, cov), "");
}

void const_death_test2(UncertainStateTest::GroupThree a,
                       Eigen::MatrixXd& cov)
{
  Lie::UncertainState<UncertainStateTest::GroupThree> state(std::make_tuple(a), cov);
}

TEST_F(UncertainStateTest, ConstructionDeathTest2)
{
  GroupThree a;

  Eigen::MatrixXd cov(2,2);
  cov <<
      1, 2,
      2, 4;

  ASSERT_DEATH(const_death_test2(a, cov), "");
}

TEST_F(UncertainStateTest, IndividualMarginalTest1)
{
  GroupOne a;
  GroupOne b;
  GroupOne c;

  Eigen::MatrixXd cov(3,3);
  cov <<
      1, 2, 3,
      2, 4, 5,
      3, 5, 6;

  typedef Lie::UncertainState<GroupOne, GroupOne, GroupOne> StateT;
  StateT state(std::make_tuple(a, b, c), cov);

  Eigen::MatrixXd marg_a = state.marginal_cov(0);
  Eigen::MatrixXd marg_b = state.marginal_cov(1);
  Eigen::MatrixXd marg_c = state.marginal_cov(2);

  Eigen::MatrixXd true_a(1, 1);
  true_a << 1;
  Eigen::MatrixXd true_b(1, 1);
  true_b << 4;
  Eigen::MatrixXd true_c(1, 1);
  true_c << 6;
  
  ASSERT_MATRICES_APPROX_EQ(marg_a, true_a);
  ASSERT_MATRICES_APPROX_EQ(marg_b, true_b);
  ASSERT_MATRICES_APPROX_EQ(marg_c, true_c);
}

TEST_F(UncertainStateTest, IndividualMarginalDeathTest1)
{
  GroupOne a;
  GroupOne b;
  GroupOne c;

  Eigen::MatrixXd cov(3,3);
  cov <<
      1, 2, 3,
      2, 4, 5,
      3, 5, 6;

  Lie::UncertainState<GroupOne, GroupOne, GroupOne>
      state(std::make_tuple(a, b, c), cov);

  ASSERT_DEATH(Eigen::MatrixXd marg_a = state.marginal_cov(3), "");
}

TEST_F(UncertainStateTest, IndividualMarginalDeathTest2)
{
  GroupOne a;
  GroupOne b;
  GroupOne c;

  Eigen::MatrixXd cov(3,3);
  cov <<
      1, 2, 3,
      2, 4, 5,
      3, 5, 6;

  Lie::UncertainState<GroupOne, GroupOne, GroupOne>
      state(std::make_tuple(a, b, c), cov);

  ASSERT_DEATH(Eigen::MatrixXd marg_a = state.marginal_cov(-1), "");
}

TEST_F(UncertainStateTest, IndividualMarginalTest2)
{
  GroupOne a;
  GroupTwo b;

  Eigen::MatrixXd cov(3,3);
  cov <<
      1, 2, 3,
      2, 4, 5,
      3, 5, 6;

  Lie::UncertainState<GroupOne, GroupTwo> state(std::make_tuple(a, b), cov);

  Eigen::MatrixXd marg_a = state.marginal_cov(0);
  Eigen::MatrixXd marg_b = state.marginal_cov(1);

  Eigen::MatrixXd true_a(1, 1);
  true_a << 1;
  Eigen::MatrixXd true_b(2, 2);
  true_b <<
      4, 5,
      5, 6;
  
  ASSERT_MATRICES_APPROX_EQ(marg_a, true_a);
  ASSERT_MATRICES_APPROX_EQ(marg_b, true_b);
}

TEST_F(UncertainStateTest, JointMarginalTest1)
{
  GroupOne a;
  GroupOne b;
  GroupOne c;

  Eigen::MatrixXd cov(3,3);
  cov <<
      1, 2, 3,
      2, 4, 5,
      3, 5, 6;

  Lie::UncertainState<GroupOne, GroupOne, GroupOne> state(std::make_tuple(a, b, c), cov);

  Eigen::MatrixXd marg_ab = state.marginal_cov({0, 1});
  Eigen::MatrixXd marg_ac = state.marginal_cov({0, 2});
  Eigen::MatrixXd marg_bc = state.marginal_cov({1,2});
  Eigen::MatrixXd marg_ba = state.marginal_cov({1, 0});

  Eigen::MatrixXd true_ab(2, 2);
  true_ab <<
      1, 2,
      2, 4;
  Eigen::MatrixXd true_ac(2, 2);
  true_ac <<
      1, 3,
      3, 6;
  Eigen::MatrixXd true_bc(2, 2);
  true_bc <<
      4, 5,
      5, 6;

  Eigen::MatrixXd true_ba(2, 2);
  true_ba <<
      4, 2,
      2, 1;  
  
  ASSERT_MATRICES_APPROX_EQ(marg_ab, true_ab);
  ASSERT_MATRICES_APPROX_EQ(marg_ac, true_ac);
  ASSERT_MATRICES_APPROX_EQ(marg_bc, true_bc);
  ASSERT_MATRICES_APPROX_EQ(marg_ba, true_ba);
}

TEST_F(UncertainStateTest, JointMarginalTest2)
{
  GroupOne a;
  GroupTwo b;
  GroupOne c;

  Eigen::MatrixXd cov(4,4);
  cov <<
      1, 2, 3, 7, 
      2, 4, 5, 8,
      3, 5, 6, 9,
      7, 8, 9, 10;

  Lie::UncertainState<GroupOne, GroupTwo, GroupOne> state(std::make_tuple(a, b, c), cov);

  Eigen::MatrixXd marg_a = state.marginal_cov(0);
  Eigen::MatrixXd marg_b = state.marginal_cov(1);
  Eigen::MatrixXd marg_c = state.marginal_cov(2);

  Eigen::MatrixXd true_a(1, 1);
  true_a << 1;
  Eigen::MatrixXd true_b(2, 2);
  true_b <<
      4, 5,
      5, 6;
  Eigen::MatrixXd true_c(1, 1);
  true_c << 10;
  
  ASSERT_MATRICES_APPROX_EQ(marg_a, true_a);
  ASSERT_MATRICES_APPROX_EQ(marg_b, true_b);
  ASSERT_MATRICES_APPROX_EQ(marg_c, true_c);

  Eigen::MatrixXd marg_ab = state.marginal_cov({0, 1});
  Eigen::MatrixXd marg_ac = state.marginal_cov({0, 2});
  Eigen::MatrixXd marg_bc = state.marginal_cov({1, 2});
  Eigen::MatrixXd marg_ba = state.marginal_cov({1, 0});

  Eigen::MatrixXd true_ab(3, 3);
  true_ab <<
      1, 2, 3, 
      2, 4, 5,
      3, 5, 6;
  Eigen::MatrixXd true_ac(2, 2);
  true_ac <<
      1, 7,
      7, 10;
  Eigen::MatrixXd true_bc(3, 3);
  true_bc <<
      4, 5, 8,
      5, 6, 9,
      8, 9, 10;

  Eigen::MatrixXd true_ba(3, 3);
  true_ba <<
      4, 5, 2, 
      5, 6, 3,
      2, 3, 1;  


  ASSERT_MATRICES_APPROX_EQ(marg_ab, true_ab);
  ASSERT_MATRICES_APPROX_EQ(marg_ac, true_ac);
  ASSERT_MATRICES_APPROX_EQ(marg_bc, true_bc);
  ASSERT_MATRICES_APPROX_EQ(marg_ba, true_ba);
}


TEST_F(UncertainStateTest, JointMarginalDeathTest1)
{
  GroupOne a;
  GroupOne b;
  GroupOne c;

  Eigen::MatrixXd cov(3,3);
  cov <<
      1, 2, 3,
      2, 4, 5,
      3, 5, 6;

  Lie::UncertainState<GroupOne, GroupOne, GroupOne> state(std::make_tuple(a, b, c), cov);

  ASSERT_DEATH(Eigen::MatrixXd marg_ab = state.marginal_cov({0, -1}), "");
}

TEST_F(UncertainStateTest, JointMarginalDeathTest2)
{
  GroupOne a;
  GroupOne b;
  GroupOne c;

  Eigen::MatrixXd cov(3,3);
  cov <<
      1, 2, 3,
      2, 4, 5,
      3, 5, 6;

  Lie::UncertainState<GroupOne, GroupOne, GroupOne> state(std::make_tuple(a, b, c), cov);

  ASSERT_DEATH(Eigen::MatrixXd marg_ab = state.marginal_cov({3}), "");
}


class UncertainGroupElementRefTest : public ::testing::Test {
 public:
  class GroupOne : public Lie::LieGroup<GroupOne, 1>
  {
   public:
    int val_;
    GroupOne() : val_(0){}
    GroupOne(int val) : val_(val){}

    GroupOne operator*(const GroupOne& rhs) const
    {
      GroupOne result(this->val_ * rhs.val_);
      return result;
    }

    static
    Eigen::MatrixXd Ad(const GroupOne& g)
    {
      auto one = Eigen::MatrixXd::Constant(1, 1, 1);
      return one;
    }
  };

  class GroupTwo : public Lie::LieGroup<GroupTwo, 2>
  {
   public:
    int val_;
    GroupTwo() : val_(0){}
    GroupTwo(int val) : val_(val){}
  };
};

TEST_F(UncertainGroupElementRefTest, MuTest1)
{
  GroupOne a(1);
  GroupTwo b(2);

  Eigen::MatrixXd cov(3,3);
  cov <<
      1, 2, 3,
      2, 4, 5,
      3, 5, 6;

  typedef Lie::UncertainState<GroupOne, GroupTwo> StateT;

  auto state = std::make_shared<StateT >(
      std::make_tuple(a, b),
      cov);

  Lie::UncertainGroupElementRef<GroupOne, StateT>  a_ref(state, 0);
  Lie::UncertainGroupElementRef<GroupTwo, StateT>  b_ref(state, 1);  

  ASSERT_EQ(a_ref.mu().val_, 1);
  ASSERT_EQ(b_ref.mu().val_, 2);
}

TEST_F(UncertainGroupElementRefTest, ComposeTest1)
{
  GroupOne a(1);
  GroupOne b(2);

  Eigen::MatrixXd cov(2,2);
  cov <<
      1, 2,
      2, 4;

  typedef Lie::UncertainState<GroupOne, GroupOne> StateT;

  auto state = std::make_shared<StateT >(
      std::make_tuple(a, b),
      cov);

  Lie::UncertainGroupElementRef<GroupOne, StateT>  a_ref(state, 0);
  Lie::UncertainGroupElementRef<GroupOne, StateT>  b_ref(state, 1);  

  auto result = Lie::compose(a_ref, b_ref);
}


TEST_F(UncertainGroupElementRefTest, FactoryAndIndepTest1)
{
  GroupOne a(1);
  GroupOne b(2);

  Eigen::MatrixXd cov(2,2);
  cov <<
      1, 2,
      2, 4;


  auto uncertain_elements = Lie::make_uncertain_state(
      std::make_tuple(a, b),
      cov);
  auto a_ref = std::get<0>(uncertain_elements);
  auto b_ref = std::get<1>(uncertain_elements);

  
  ASSERT_EQ(a_ref.mu().val_, 1);
  ASSERT_EQ(b_ref.mu().val_, 2);
  ASSERT_MATRICES_APPROX_EQ(a_ref.marginal_cov(), cov.block(0,0,1,1));
  ASSERT_MATRICES_APPROX_EQ(b_ref.marginal_cov(), cov.block(1,1,1,1));

  ASSERT_FALSE(a_ref.is_independent_of(b_ref));

  auto state = a_ref.get_raw_underlying_state();

  ASSERT_MATRICES_APPROX_EQ(state->marginal_cov({0,1}), cov);
}

TEST_F(UncertainGroupElementRefTest, FactoryAndIndepTest2)
{
  GroupOne a(1);
  GroupTwo b(2);

  Eigen::MatrixXd cov(3,3);
  cov <<
      1, 2, 3,
      2, 4, 5,
      3, 5, 6;


  auto uncertain_elements = Lie::make_uncertain_state(
      std::make_tuple(a, b),
      cov);
  auto a_ref = std::get<0>(uncertain_elements);
  auto b_ref = std::get<1>(uncertain_elements);

  
  ASSERT_EQ(a_ref.mu().val_, 1);
  ASSERT_EQ(b_ref.mu().val_, 2);
  ASSERT_MATRICES_APPROX_EQ(a_ref.marginal_cov(), cov.block(0,0,1,1));
  ASSERT_MATRICES_APPROX_EQ(b_ref.marginal_cov(), cov.block(1,1,2,2));

  ASSERT_FALSE(a_ref.is_independent_of(b_ref));
  
  auto state = a_ref.get_raw_underlying_state();
  
  ASSERT_MATRICES_APPROX_EQ(state->marginal_cov({0,1}), cov);
}

TEST_F(UncertainGroupElementRefTest, FactoryAndIndepTest3)
{
  GroupOne a(1);
  GroupTwo b(2);

  Eigen::MatrixXd cov_a(1,1);
  cov_a << 1;

  Eigen::MatrixXd cov_b(2,2);
  cov_b <<
      4, 5,
      5, 6;

  auto a_ref = Lie::make_uncertain_state(a, cov_a);
  auto b_ref = Lie::make_uncertain_state(b, cov_b);

  
  ASSERT_EQ(a_ref.mu().val_, 1);
  ASSERT_EQ(b_ref.mu().val_, 2);
  ASSERT_MATRICES_APPROX_EQ(a_ref.marginal_cov(), cov_a);
  ASSERT_MATRICES_APPROX_EQ(b_ref.marginal_cov(), cov_b);

  ASSERT_TRUE(a_ref.is_independent_of(b_ref));
}



TEST_F(UncertainGroupElementRefTest, FactoryDeathTest1)
{
  GroupOne a(1);
  GroupTwo b(2);
  GroupOne c(1);

  Eigen::MatrixXd cov(3,3);
  cov <<
      1, 2, 3,
      2, 4, 5,
      3, 5, 6;


  ASSERT_DEATH(Lie::make_uncertain_state(std::make_tuple(a, b, c),
                                         cov),
               "");
  // Dies because covariance size doesn't match total algebra dimension
}

