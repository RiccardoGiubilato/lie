//ellipse.hpp
//Conic detection for ellipses

#ifndef CONIC_HPP
#define CONIC_HPP

#include <opencv/cv.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "mr/eigen_util.hpp"

namespace MR
{
  class ELLIPSE
  {
  public:

    /**
     * Converts the bounding rotated rect returned from fitEllipse to the CONIC 
     * Matrix C, where 
     *
     *     [[A, B, D]
     * C =  [B, C, E]
     *      [D, E, F]];
     *
     * s.t. the quadratic form of the ellipse is:
     * 
     * x^T * C * x = 0, where x = [x_e, y_e, 1]^T, and (x_e, y_e) is any point on the ellipse
     *
     * thus the implicit equation is:
     *
     * A*x_e^2 + 2*B*x_e*y_e + C*y_e^2 + 2*D*x_e + 2*E*y_e + F = 0
     **/
    static Eigen::MatrixXd rotated_rect_to_Q(cv::RotatedRect& ellipse);
  };

  class DUAL_ELLIPSE
  {
  private:
    Eigen::MatrixXd C_star;
    double scale;
    double x_shift;
    double y_shift;
  public:
    /**
     * This constructor fits a dual ellipse to the roi image passed in.
     * The method used is the gradiant method presented by:
     * Jean-Nicolas Oullet and Patrick Hebert in:
     * "Precise ellipse estimation without contour point extraction"
     */
    DUAL_ELLIPSE(cv::Mat& roi, cv::Point2f& aprox_center);
    ~DUAL_ELLIPSE();

    cv::Point2f center();
  };
}

#endif
