//stat.hpp

#ifndef STAT_HPP
#define STAT_HPP

#include "eigen_util.hpp"

namespace MR
{ 
  namespace STAT
  {
    /**
     * Returns a random but correct covariance matrix of size n x n.
     **/
    Eigen::MatrixXd rand_cov(int n);

    /** 
     * Returns a multivariate gaussian drawn from the provided distribution.
     **/
    Eigen::VectorXd sample_mvg(const Eigen::VectorXd& mu, const Eigen::MatrixXd& Sigma);
  
    /**
     * Returns N samples drawn from the multivariate gaussian distribution.
     *
     * Samples are returned as the columns of a MatrixXd
     **/
    Eigen::MatrixXd sample_mvg(const Eigen::VectorXd& mu, const Eigen::MatrixXd& Sigma, int N);

    /**
     * Calculates the mean of a colection of multivariate gaussian random samples.
     * 
     * The samples are contained in the columns of Samples.
     * If angle_map is NULL all values are treated as non angles
     **/
    Eigen::VectorXd mvg_mu(const Eigen::MatrixXd& Samples, const bool angle_map[]=NULL);
  
    /**
     * Calculates the covariance of a colection of multivariate gaussian random samples.
     * 
     * The samples are contained in the columns of Samples.
     * If angle_map is NULL all values are treated as non angles
     **/
    Eigen::MatrixXd mvg_cov(const Eigen::MatrixXd& Samples, const bool angle_map[]=NULL);

    /**
     * Takes the Kullback-Leibler Divergence of q from p 
     * Where q and p have the means mu_q and mu_p, and the 
     * covariances sigma_q and sigma_p respectively.
     *
     * often p is some "correct" distribution 
     * and q is some approximation of p
     *
     * Assumes they have the same dimension
     **/
    double mvg_kld(const Eigen::VectorXd& mu_q, const Eigen::MatrixXd& sigma_q, 
                   const Eigen::VectorXd& mu_p, const Eigen::MatrixXd& sigma_p,
                   const bool angle_map[]=NULL);
  
    /**
     * Returns a Vector containing the marginal standard deviation of each dimension
     * given a covariance matrix. However, this should not be used in analysis because
     * it does not take into account correlation.
     *
     * angle_map is an array of bool specifying which elements should be treated as angles
     * true=angle. These values will be returned in degrees.
     **/
    Eigen::VectorXd mrg_stddev(const Eigen::MatrixXd& Sigma, bool angle_map[]);
  };
};

#endif
