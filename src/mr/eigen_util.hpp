//eigen_util.hpp

#ifndef EIGEN_UTIL_HPP
#define EIGEN_UTIL_HPP

#include <Eigen/Core>

typedef Eigen::Matrix<double, 6, 6> Matrix6d;
typedef Eigen::Matrix<double, 6, 1> Vector6d;


#endif
