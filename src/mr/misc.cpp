#include <cmath>
#include <iostream>

#include "misc.hpp"

double minimized_angle(double angle)
{
  while(angle < -M_PI)
    angle = angle + 2.0*M_PI;
  
  while(angle >= M_PI)
    angle = angle - 2.0*M_PI;

  return angle;
}

// If angle_map is NULL it assumes none are angles
Eigen::VectorXd minimized_angle_vec(const Eigen::VectorXd& angles, bool angle_map[])
{
  int n = angles.rows();
  
  if(angle_map==NULL)
    return angles;

  Eigen::VectorXd minAngles(n);

  for(int i=0;i<n;i++){
    if(angle_map[i])
      minAngles(i) = minimized_angle(angles(i));
    else
      minAngles(i) = angles(i);
  } 

  return minAngles;
}

double circular_mean (const Eigen::VectorXd& samples)
{
  double n = (double)samples.rows();
  double sin_sum = (samples.array().sin()*(1.0/n)).sum();
  double cos_sum = (samples.array().cos()*(1.0/n)).sum();
  return atan2(sin_sum, cos_sum);
}

double circular_mean (const Eigen::VectorXd& samples, const Eigen::VectorXd& weights){
  double sin_sum = (samples.array().sin() * weights.array()).sum();
  double cos_sum = (samples.array().cos() * weights.array()).sum();
  return atan2(sin_sum, cos_sum);
}




