
#include "diff.hpp"

Eigen::MatrixXd MR::DIFF::num_jacob_central( OutputVector (*f)(InputVectorRef, void *), InputVectorRef x, void * args, double del){
  int num_vars = x.rows();

  Eigen::VectorXd in1(num_vars);
  Eigen::VectorXd in2(num_vars);
  Eigen::VectorXd out1;
  Eigen::VectorXd out2;
  Eigen::MatrixXd J;
  bool init = false;

  for(int i=0; i<num_vars; i++){
    in1 = x;
    in2 = x;
    in1(i) = in1(i) + del;
    in2(i) = in2(i) - del;
    
    out1 = f(in1, args);
    out2 = f(in2, args);
    
    if(!init)
      J.resize(out1.rows(), num_vars);

    J.col(i) = (out1 - out2)/(del*2);
  }

  return J;
}
