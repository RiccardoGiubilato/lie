#include <iostream>
#include <stdlib.h>
#include <cmath>

#include <opencv/cv.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "mr/camera.hpp"

#include "mr/ct.hpp"
#include "mr/stat.hpp"
#include "mr/diff.hpp"

void rt_to_pose(Eigen::VectorXd& pose, cv::Mat rvec, cv::Mat tvec)
{
  cv::Mat rot(3,3, cv::DataType<double>::type);
  cv::Rodrigues(rvec, rot);

  Eigen::MatrixXd R(3,3);
  R << 
    rot.at<double>(0,0), rot.at<double>(0,1), rot.at<double>(0,2),
    rot.at<double>(1,0), rot.at<double>(1,1), rot.at<double>(1,2),
    rot.at<double>(2,0), rot.at<double>(2,1), rot.at<double>(2,2);

  Eigen::VectorXd rph(3);
  rph = MR::SO3::rot2rph(R);
  
  // Setup Pose 
  pose(0) = tvec.at<double>(0);
  pose(1) = tvec.at<double>(1);
  pose(2) = tvec.at<double>(2);
  pose(3) = rph(0);
  pose(4) = rph(1);
  pose(5) = rph(2);
} 

void MR::CAMERA::setup_reprojection_args(reprojection_args_t *args, Eigen::MatrixXd& points_3d, camera_cal_t* calib)
{
  assert(points_3d.cols() == 3);

  args->K = calib->K;
  args->points3d = points_3d;
}

Eigen::VectorXd MR::CAMERA::reproject_points(const Eigen::VectorXd& x_cam2tag, void *reprojection_args)
{
  reprojection_args_t *args = (reprojection_args_t *)reprojection_args;
  Eigen::MatrixXd& K = args->K;
  Points3d& points3d = args->points3d;

  Eigen::VectorXd x_cam2tag_x_tag2pnt(12);
  x_cam2tag_x_tag2pnt.head(6) = x_cam2tag;
  x_cam2tag_x_tag2pnt.tail(3) << 0, 0, 0;
  Eigen::VectorXd x_cam2pnt(6);
  Eigen::Vector3d xyz_i;

  int num_points = points3d.rows();
  Eigen::VectorXd points2d(num_points*2);

  for(int i=0;i<num_points;i++){
    x_cam2tag_x_tag2pnt.segment(6, 3) = points3d.row(i).transpose();
    x_cam2pnt = MR::SSC::head2tail_fp(x_cam2tag_x_tag2pnt, NULL);
    xyz_i = K*x_cam2pnt.head(3);
    points2d(2*i) = xyz_i(0)/xyz_i(2);     //u
    points2d(2*i + 1) = xyz_i(1)/xyz_i(2); //v
  }

  return points2d;
}

std::vector<cv::Point3f> generate_3d_points_vector(Eigen::MatrixXd& points3d){
  assert(points3d.cols()==3);
  
  std::vector<cv::Point3f> points;
  int num_points = points3d.rows();
  
  for(int i=0;i<num_points;i++){
    points.push_back(cv::Point3f(points3d(i, 0), points3d(i, 1), points3d(i, 2)) );
  }
  return points;
}

std::vector<cv::Point2f> generate_2d_points_vector(const Eigen::VectorXd& pnts)
{
  std::vector<cv::Point2f> points;
  int num_points = pnts.rows()/2;

  int i;
  for (i=0;i<num_points;i++){
    double x = pnts(i*2); //p[i][0];
    double y = pnts(i*2 + 1); //p[i][1]; 

    points.push_back(cv::Point2f(x,y)); 
  }
  return points;
}

  void MR::CAMERA::setup_solve_pose_args(solve_pose_args_t *args, Eigen::MatrixXd& points_3d, camera_cal_t *calib)
{
  assert(points_3d.cols() == 3);

  //args->image_points = generate_2d_points_vector(p, num_points);
  args->object_points = generate_3d_points_vector(points_3d);
  
  // Setup the intrinsic calibration matrix K
  cv::Mat& cameraMatrix = args->cameraMatrix;
  cameraMatrix.create(3,3,cv::DataType<double>::type);
  cv::setIdentity(cameraMatrix);
  cameraMatrix.at<double>(0,0) = calib->K(0,0);
  cameraMatrix.at<double>(0,1) = calib->K(0,1);
  cameraMatrix.at<double>(0,2) = calib->K(0,2);
  cameraMatrix.at<double>(1,0) = calib->K(1,0);
  cameraMatrix.at<double>(1,1) = calib->K(1,1);
  cameraMatrix.at<double>(1,2) = calib->K(1,2);
  cameraMatrix.at<double>(2,0) = calib->K(2,0);
  cameraMatrix.at<double>(2,1) = calib->K(2,1);
  cameraMatrix.at<double>(2,2) = calib->K(2,2);

  // Set up the distortion coefficients
  cv::Mat& distCoeffs = args->distCoeffs;
  if(calib->dist_coeffs.rows() == 0)
    distCoeffs.create(0,1, cv::DataType<double>::type);
  else{
    distCoeffs.create(calib->dist_coeffs.rows(),1, cv::DataType<double>::type);
    for(int i=0;i<calib->dist_coeffs.rows();i++)
      distCoeffs.at<double>(i) = calib->dist_coeffs(i);
  }
}

Eigen::VectorXd MR::CAMERA::solve_pose_open_cv(const Eigen::VectorXd& pixel_corners, void *args)
{
  std::vector<cv::Point2f> image_points;
  image_points = generate_2d_points_vector(pixel_corners);
  
  solve_pose_args_t *sp = (solve_pose_args_t *)args;
  
  // Set up the output vectors
  cv::Mat rvec(3,1, cv::DataType<double>::type);
  cv::Mat tvec(3,1, cv::DataType<double>::type);
  
  //Solve for Camera Pose using Levenberg-Marquardt
  cv::solvePnP(sp->object_points, image_points, sp->cameraMatrix, sp->distCoeffs, rvec, tvec);
  
  Eigen::VectorXd x_cam2tag(6);
  rt_to_pose(x_cam2tag, rvec, tvec);
  
  return x_cam2tag;
}


/*************************************************
 * Finding the Uncertainty of a Pose Measurement *
 *************************************************/

void MR::CAMERA::mc_uncertainty_characterization(MR::CAMERA::mc_return_t *mc_ret, 
                                                 const Eigen::VectorXd& pix_points, 
                                                 const Eigen::MatrixXd& Sigma, 
                                                 MR::CAMERA::solve_pose_args_t *sp_args, int N){ 
  Eigen::MatrixXd samples;
  samples = MR::STAT::sample_mvg(pix_points, Sigma, N);

  Eigen::MatrixXd pose_samples(6, N);

  for(int i=0;i<N;i++){
    Eigen::VectorXd pose;
    pose = MR::CAMERA::solve_pose_open_cv(samples.col(i), (void *)sp_args);
    //std::cout << "Pose " << i << ": " << pose.transpose() << "\n";
    pose_samples.col(i) = pose;
  }

  bool angle_map[6] = {false, false, false, true, true, true};
  mc_ret->mu_prime = MR::STAT::mvg_mu(pose_samples, angle_map);

  mc_ret->sigma_prime = MR::STAT::mvg_cov(pose_samples, angle_map);
}

Matrix6d MR::CAMERA::covariance_backwards_projection(Eigen::VectorXd& x_cam2tag, 
                                                     const Eigen::MatrixXd& Sigma, 
                                                     MR::CAMERA::reprojection_args_t *rp_args){
  
  Eigen::MatrixXd J = MR::DIFF::num_jacob_central(MR::CAMERA::reproject_points, x_cam2tag, (void *)rp_args);

  Matrix6d backwards_prop_info_matrix = J.transpose()*Sigma.inverse()*J;
  
  Matrix6d pose_cov = backwards_prop_info_matrix.inverse();
  //std::cout << std::scientific << "Pose Cov: \n" << pose_cov << "\n";

  //Vector6d marg_stddev = pose_cov.diagonal().array().sqrt();
  //marg_stddev.tail(3) = marg_stddev.tail(3)*180/3.14159;
  //std::cout << std::fixed << "Marg Std Dev: \n" << marg_stddev << "\n";

  return pose_cov;
}

void MR::CAMERA::run_unscented_trans(MR::UT::ut_return_t *ut_ret, Eigen::VectorXd& pixel_points, 
                                     const Eigen::MatrixXd& Sigma, 
                                     MR::CAMERA::solve_pose_args_t *sp_args, 
                                     double alpha, double kappa){
  //std::cout << "Unscented Transform----\n";

  MR::UT::ut_t ut;
  MR::UT::unscented_transform(&ut, pixel_points, Sigma, alpha, kappa);

  bool angle_map[6] = {false, false, false, true, true, true};
  MR::UT::unscented_func(ut_ret, MR::CAMERA::solve_pose_open_cv, &ut, sp_args, angle_map);
  //  std::cout << std::scientific<< "Sigma Points:\n" << ut.sigma_points << std::endl;
  //std::cout << std::scientific<< "Mean Weights:\n" << ut.mean_weight.transpose() << std::endl;
  //std::cout << std::scientific<< "Cov Weights:\n" << ut.cov_weight.transpose() << std::endl;

  //Eigen::MatrixXd& pose_cov = ut_ret->sigma_prime;
  //std::cout << std::scientific << "Pose Cov:\n" << pose_cov << "\n";

  //Vector6d marg_stddev = pose_cov.diagonal().array().sqrt();
  //marg_stddev.tail(3) = marg_stddev.tail(3)*180/3.14159;
  //std::cout << std::fixed << "Marg Std Dev: \n" << marg_stddev << "\n";
}

