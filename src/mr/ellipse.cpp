#include <iostream>
#include <cmath>

#include <Eigen/Dense>

#include <opencv2/highgui/highgui.hpp>

#include "mr/ellipse.hpp"

Eigen::MatrixXd MR::ELLIPSE::rotated_rect_to_Q(cv::RotatedRect& ellipse)
{
  float theta = ellipse.angle*M_PI/180.0;
  cv::Size2f size = ellipse.size;
  cv::Point2f center = ellipse.center;
  float x_c = center.x;
  float y_c = center.y;
  float a = MAX(size.height, size.width)/2.0;
  float b = MIN(size.height, size.width)/2.0;
  
  float a_2 = pow(a,2);
  float b_2 = pow(b,2);
  float s_th= sin(theta);
  float c_th= cos(theta);
  float s_2 = pow(s_th,2);
  float c_2 = pow(c_th,2);

  float A = a_2*s_2 + b_2*c_2;
  float B = 2*(b_2 - a_2)*s_th*c_th;
  float C = a_2*c_2 + b_2*s_2;
  float D = -2*A*x_c - B*y_c;
  float E = -B*x_c - 2*C*y_c;
  float F = A*pow(x_c,2) + B*x_c*y_c + C*pow(y_c,2) - a_2*b_2;

  Eigen::MatrixXd Q(3,3);
  Q <<     A,      B/2.0, D/2.0,
       B/2.0,          C, E/2.0,
       D/2.0,      E/2.0, F     ;
  
  return Q;
}

/**
 * This constructor fits a dual ellipse to the roi image passed in.
 * The method used is the gradiant method presented by:
 * Jean-Nicolas Oullet and Patrick Hebert in:
 * "Precise ellipse estimation without contour point extraction"
 */
MR::DUAL_ELLIPSE::DUAL_ELLIPSE(cv::Mat& roi, cv::Point2f& aprox_center)
{
  cv::Size img_size = roi.size();

  //Find the gradiants
  cv::Mat Ix, Iy;
  int sobel_ddepth = CV_64F;
  int sobel_scale = 1;
  int sobel_delta = 0;

  cv::Sobel(roi, Ix, sobel_ddepth, 1, 0, 5, sobel_scale, sobel_delta, IPL_BORDER_REPLICATE);
  cv::Sobel(roi, Iy, sobel_ddepth, 0, 1, 5, sobel_scale, sobel_delta, IPL_BORDER_REPLICATE);
  /*
  cv::namedWindow( "Input", CV_WINDOW_AUTOSIZE );
  cv::imshow( "Input", roi );
  cv::waitKey(50);

  cv::namedWindow( "Ix", CV_WINDOW_AUTOSIZE );
  cv::imshow( "Ix", Ix );
  cv::waitKey(50);

  cv::namedWindow( "Iy", CV_WINDOW_AUTOSIZE );
  cv::imshow( "Iy", Iy );
  cv::waitKey(50);*/

  //Find Lines - Shift
  Eigen::MatrixXd lines(3, img_size.height*img_size.width);
  Eigen::RowVectorXd weights(img_size.height*img_size.width);
  Eigen::RowVectorXd valid(img_size.height*img_size.width);
  
  for(int v=0;v<img_size.height;v++) { //y
      int shifted_v = v - aprox_center.y;
      for(int u=0;u<img_size.width;u++){ //x
        int shifted_u = u - aprox_center.x;

        double Iui = Ix.at<double>(u, v);
        double Ivi = Iy.at<double>(u, v);

        double thresh2 = 0.001;
        if(abs(Iui) < thresh2) Iui = 0;
        if(abs(Ivi) < thresh2) Ivi = 0;
        
        double thresh = 10000;
        if(abs(Iui) > thresh) Iui = INFINITY;
        if(abs(Ivi) > thresh) Ivi = INFINITY;


        //    std::cout << "Iui: " << Iui << " Ivi: " << Ivi << "\n";

        lines(0,v*img_size.width + u) = Iui;
        lines(1,v*img_size.width + u) = Ivi;
        lines(2,v*img_size.width + u) = -(Iui*shifted_u + Ivi*shifted_v);

        weights(v*img_size.width + u) = pow(Iui,2) + pow(Ivi,2);

        if( (Iui == 0 && Ivi == 0) || isnan(Iui) || isnan(Ivi) || isinf(Iui) || isinf(Ivi))
          valid(v*img_size.width + u) = 0;
        else
          valid(v*img_size.width + u) = 1; 

        //std::cout << "line: " << lines.col(v*img_size.width + u).transpose() << "\n";
        //std::cout << "wi: " << weights(v*img_size.width + u) << "\n";
        //std::cout << "valid: " << valid(v*img_size.width + u) << "\n";
      }
  }
 
  
  //Normalization - For numerical reasons, shift the aprox center to
  // be at the origin and scale so that the avg distance from a line 
  // to the origin is sqrt(2)
  double total_dist = 0;
  for(int i=0;i<lines.cols();i++){
    if(valid(i) == 1){
      Eigen::Vector3d li = lines.col(i);
      total_dist += abs(li(2))/sqrt(pow(li(0),2) + pow(li(1),2));
    }
  }
  int num_valid_lines = valid.sum();
  double avg_dist = total_dist/(double)num_valid_lines;
  double scale_tmp = sqrt(2.0)/avg_dist;

  for(int v=0;v<img_size.height;v++) { //y
    int new_v = (v - aprox_center.y)*scale_tmp;
    for(int u=0;u<img_size.width;u++){ //x

      if(valid(v*img_size.width + u) == 0)
        continue;

      int new_u = (u - aprox_center.x)*scale_tmp;

      double Iui = (double)Ix.at<double>(u, v);
      double Ivi = (double)Iy.at<double>(u, v);

      lines(2,v*img_size.width + u) = -(Iui*new_u + Ivi*new_v);
    }
  }
  
  //Calculate matrices for least squares estimation
  Eigen::MatrixXd A;
  A = Eigen::MatrixXd::Zero(5,5);
  Eigen::VectorXd b;
  b = Eigen::VectorXd::Zero(5);

  for(int i=0;i<lines.cols();i++){
    if(valid(i) == 0)
      continue;

    Eigen::Vector3d li = lines.col(i);
    Eigen::VectorXd K_prime(5);
    K_prime << pow(li(0),2), li(0)*li(1), pow(li(1),2), li(0)*li(2), li(1)*li(2);
    
    A += pow(weights(i), 2)*K_prime*K_prime.transpose();
    b += -pow(weights(i), 2)*K_prime*pow(li(2), 2);
    //std::cout << "li: " << li.transpose() << "\n";
    //std::cout << "wi^2: " << pow(weights(i),2) << "\n";
    //std::cout << "K': " << K_prime.transpose() << "\n";
  }
  //std::cout << "A:\n" << A << "\nb:\n" << b << "\n";

  //Solve for ellipse
  Eigen::VectorXd Theta_prime = A.colPivHouseholderQr().solve(b);
  Eigen::MatrixXd C_tmp(3,3);
  C_tmp << 
    Theta_prime(0),      Theta_prime(1)/2.0, Theta_prime(3)/2.0,
    Theta_prime(1)/2.0,  Theta_prime(2),     Theta_prime(4)/2.0,
    Theta_prime(3)/2.0,  Theta_prime(4)/2.0, 1                  ;
  C_star = C_tmp;
  //  std::cout << "C_star: \n" << C_star << "\n";

  //std::cout << "Num Valid Lines: " << num_valid_lines << "\n";
  //std::cout << "Avg Dist: " << avg_dist << "\n";
  //std::cout << "Scale tmp: " << scale_tmp << "\n";

  //Save offsets for future use
  scale = 1.0/scale_tmp;
  //  std::cout << "Scale:" << scale << "\n";
  x_shift = aprox_center.x;
  y_shift = aprox_center.y;


  //std::cout << "Ix:\n" << Ix << "\nIy: \n" << Iy << "\n";

  //pause();

  //exit(0);
}

MR::DUAL_ELLIPSE::~DUAL_ELLIPSE(){}

cv::Point2f MR::DUAL_ELLIPSE::center()
{
  Eigen::VectorXd pole_of_line_at_infinity = C_star.col(2);
  float u_c = pole_of_line_at_infinity(0)/pole_of_line_at_infinity(2);
  float v_c = pole_of_line_at_infinity(1)/pole_of_line_at_infinity(2);
  
  //  std::cout << "Pole pf l at infinity: " << pole_of_line_at_infinity.transpose() << "\n";
  //std::cout << "Scale: " << scale << "\n";
  u_c = u_c*scale + x_shift;
  v_c = v_c*scale + y_shift;
  
  return cv::Point2f(u_c, v_c);
}
