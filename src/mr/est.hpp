//est.hpp

#ifndef EST_HPP
#define EST_HPP

#include "eigen_util.hpp"
#include "diff.hpp"

namespace MR
{
  namespace EST
  {
    class LIN_ALG
    {
    public:

      /**
       * Returns (A + BCD)^(-1) given A^(-1), B, C, D
       **/
      static Eigen::MatrixXd matrix_inversion_lemma(const Eigen::MatrixXd& A_inv, 
                                                    const Eigen::MatrixXd& B, 
                                                    const Eigen::MatrixXd& C, 
                                                    const Eigen::MatrixXd& D);

    };

    class Est_Base
    {
    private:
      Est_Base();
    protected:
      int dim;

      Eigen::VectorXd x_k;
    
      bool initialized;

      bool * angle_map;

      Est_Base(int state_dim, bool angle_map[]=NULL);
      ~Est_Base();
    
    public:
      bool is_initialized() const {return initialized;}
      Eigen::VectorXd mu() const {assert(initialized); return x_k;}

      void clear();
    };

    typedef DIFF::InputVectorRef StateVector;
    typedef DIFF::OutputVector UpdatedStateVector;
    typedef DIFF::OutputVector MeasurementVector;

    typedef UpdatedStateVector (*ProcessModel)(StateVector, void *);
    typedef MeasurementVector (*ObservationModel)(StateVector, void *);

    class EKF : public Est_Base
    {
    protected:
      Eigen::MatrixXd P_k; //Covariance Matrix
      double NIS_threash;
      bool innov_bound;
      double *innov_threash;
    
    public:
      //EKF(int state_dim, bool angle_map[]=NULL);
      EKF(int state_dim, bool angle_map[]=NULL, double NIS_threashold=-1, double innovation_threashold[]=NULL);
      ~EKF();

      Eigen::MatrixXd cov() const {assert(initialized); return P_k;}

      void init(const Eigen::VectorXd& x_0, const Eigen::MatrixXd& P_0);

      //Run forward the state based on a differentiable process model
      //Q_k is the covariance of the process model noise (often due to control noise)
      void predict( ProcessModel f, void *args, const Eigen::MatrixXd& Q_k);

      //Correct the state estimate based on a differentiable observation model
      //R_k is the covariance of the observation noise
      //returns the NIS calculated from the innovation
      double update( ObservationModel h, void *args, const Eigen::VectorXd& z_k, 
                   const Eigen::MatrixXd& R_k);
    };

    class WLS : public Est_Base
    {
    protected:
      Eigen::MatrixXd Q_k; //Information Matrix at time K. (IF weight is meas info matrix)
      Eigen::MatrixXd P_k; //Covariance of state at time K. (IF weight is meas info matrix)

      Eigen::VectorXd Gamma_n; //Used for initialization (Information Vector)
    public:
      WLS(int state_dim, bool angle_map[]=NULL);
      ~WLS();

      void clear();

      void add_measurement(const Eigen::VectorXd& y_kplus1, const Eigen::MatrixXd& S_kplus1, 
                           const Eigen::MatrixXd& C_kplus1);  
    };

    class WLS_Forget : public WLS
    {
    private:
      double lambda; //The Forgetting factor
    public:
      WLS_Forget(double forgetting_factor, int state_dim, bool angle_map[]=NULL) : 
        WLS(state_dim, angle_map) { lambda = forgetting_factor;}

      ~WLS_Forget() {}

      double forgetting_factor() const {return lambda;}

      void add_measurement(const Eigen::VectorXd& y_kplus1, const Eigen::MatrixXd& S_kplus1, 
                           const Eigen::MatrixXd& C_kplus1);  
    };
  };
}
#endif
