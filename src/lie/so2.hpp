#ifndef __LIE_SO2_HPP
#define __LIE_SO2_HPP

#include <assert.h>
#include <iostream>

#include <Eigen/Core>

#include "lie.hpp"

namespace Eigen
{
typedef Matrix<double, 1, 1> Matrix1d;
typedef Matrix<double, 1, 1> Vector1d;
};

namespace Lie
{

typedef Eigen::Matrix<double, 2, 2, Eigen::RowMajor> Matrix2dRow;
typedef Eigen::Matrix<double, 2, 1, Eigen::RowMajor> Vector2dRow;


/////////////////////////////////////////
// Auxiliary Functions 
/////////////////////////////////////////
Matrix2dRow skew(double theta)
{
  Matrix2dRow v_mat;
  v_mat <<
      0, -theta,
      theta, 0;
  return v_mat;
}

//////////////////////////////////////////////////////////
// The SO2 class represents an element of the SO(2) group
//////////////////////////////////////////////////////////
class SO2 : public LieGroup<SO2, 1>
{
 private:
  Matrix2dRow R_;
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  SO2()
      : R_()
  {
    R_ = Matrix2dRow::Identity();
  }
  
  SO2(const SO2& R)
      : R_(R())
  {}
  
  SO2(Eigen::Matrix2d R)
      : R_(R)
  {}

  SO2(double theta)
      : R_()
  {
    R_ <<
        cos(theta), -sin(theta),
        sin(theta), cos(theta);
  }

  ~SO2() {}

  SO2 inverse() const
  {
    SO2 inv(this->R_.transpose());
    return inv;
  }
  
  Matrix2dRow operator()() const
  {
    return R_;
  }

  // Multiplication Action
  SO2 operator*(const SO2& rhs) const
  {
    Matrix2dRow result = (*this)()*rhs();
    SO2 R = SO2(result);
    return R;
  }
  
  static SO2 Exp(const TangentVector& v)
  {
    double theta = v(0);

    Eigen::Matrix2d R;
    R <<
        cos(theta), -sin(theta),
        sin(theta), cos(theta);

    return SO2(R);
  }

  static TangentVector Log(const SO2& g)
  {
    double theta = atan2(g()(1,0), g()(0,0));

    TangentVector w;
    w << theta;
    return w;
  }

  static Eigen::Matrix<double, 1, 1> Ad(const SO2& g)
  {
    Eigen::Matrix1d one = Eigen::Matrix1d::Identity();
    return one;
  }

  using LieGroup<SO2, 1>::inverse;
};

////////////////////////////////////
// Other Useful Operators
////////////////////////////////////
std::ostream& operator<<(std::ostream& os, const SO2& rhs)  
{
  os << rhs();
  return os;
} 

Eigen::Vector2d operator*(const SO2& lhs, const Eigen::Vector2d& rhs)
{
  Eigen::Vector2d result = lhs()*rhs;
  return result;
}

Eigen::Vector2d operator*(const SO2& lhs, const Eigen::VectorXd& rhs)
{
  assert(rhs.rows() == 2);
  
  Eigen::Vector2d result = lhs()*rhs;
  return result;
}
  



}; // namespace Lie
#endif //__LIE_SO2_HPP

