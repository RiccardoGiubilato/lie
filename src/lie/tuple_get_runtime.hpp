#ifndef __TUPLE_GET_RUNTIME_HPP
#define __TUPLE_GET_RUNTIME_HPP

#include <tuple>


////////////////
// Tuple Helper
////////////////
template <typename ReturnType, size_t I>
struct get_runtime_impl
{
    template <typename T>
    static ReturnType& get(T& tup, size_t idx)
    {
        if (idx == I - 1) return std::get<I - 1>(tup);
        else return get_runtime_impl<ReturnType, I - 1>::get(tup, idx);
    }
};

template <typename ReturnType>
struct get_runtime_impl<ReturnType, 0>
{
    template <typename T>
    static ReturnType& get(T& tup, size_t idx) { assert(false); }
};

template <typename ReturnType, typename... Ts>
ReturnType& get_runtime(std::tuple<Ts...> const& tup, size_t idx)
{
  return get_runtime_impl<ReturnType, sizeof...(Ts)>::get(tup, idx);
}

template <typename ReturnType, typename... Ts>
ReturnType& get_runtime(std::tuple<Ts...>& tup, size_t idx)
{
  return get_runtime_impl<ReturnType, sizeof...(Ts)>::get(tup, idx);
}




#endif //__TUPLE_GET_RUNTIME_HPP
