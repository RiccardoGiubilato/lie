#ifndef __LIE_SE2_HPP
#define __LIE_SE2_HPP

#include <Eigen/Core>
#include <iostream>

#include "so2.hpp"
#include "so3.hpp"

namespace Lie
{

///////////////////////////////////////////////////////////
// The SE3 class represents an element of the SE(3) group
///////////////////////////////////////////////////////////
class SE2 : public LieGroup<SE2, 3>
{
 private:
  Matrix3dRow H_;

 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  SE2()
      : H_()
  {
    H_ = Matrix3dRow::Identity();
  }
  
  SE2(const SE2& H)
      : H_(H())
  {}
  
  SE2(const Matrix3dRow& H)
      : H_(H)
  {}

  SE2(const SO2& R,
      const Eigen::Vector2d& t)
      : H_()
  {
    H_.block(0, 0, 2, 2) = R();
    H_.block(0, 2, 2, 1) = t;

    H_(2, 0) = 0;
    H_(2, 1) = 0;
    H_(2, 2) = 1;
  }
  
  SE2(double x,
      double y,
      double h)
      : H_()
  {
    
    SO2 R(h);
    H_.block(0, 0, 2, 2) = R();
    H_(0, 2) = x;
    H_(1, 2) = y;

    H_(2, 0) = 0;
    H_(2, 1) = 0;
    H_(2, 2) = 1;
  }
  
  ~SE2() {}

  SO2 R() const
  {
    SO2 R(H_.block(0, 0, 2, 2));
    return R;
  }

  Eigen::Vector2d t() const
  {
    Eigen::Vector2d t = H_.block(0, 2, 2, 1);
    return t;
  }
  
  SE2 inverse() const
  {
    SE2 inv(this->R().inverse(),
            this->R().inverse()() * this->t() * (-1));
    return inv;
  }

  // Multiplication Action
  SE2 operator*(const SE2& rhs) const 
  {
    Matrix3dRow result = (*this)()*rhs();
    SE2 H = SE2(result);
    return H;
  }

  Matrix3dRow operator()() const
  {
    return H_;
  }

  static SE2 Exp(const TangentVector& xi)
  {
    double theta = xi(2);
    Eigen::MatrixXd Theta_skew(2,2);
    Theta_skew = skew(theta);
    
    Eigen::MatrixXd V  = Eigen::MatrixXd::Identity(2,2);
    if(theta < 0.000001)
    {
      V = V + (1/2.0)*Theta_skew + (1/6.0)*Theta_skew*Theta_skew + (1/24.0)*Theta_skew*Theta_skew*Theta_skew;
    }
    else
    {
      V(0,0) = (1/theta)*sin(theta);
      V(0,1) = -(1/theta)*(1 - cos(theta));
      V(1,0) = (1/theta)*(1 - cos(theta));
      V(1,1) = (1/theta)*sin(theta);
    }

    Eigen::MatrixXd ExpDelta(3,3);
    ExpDelta <<
        cos(theta), -sin(theta), 0,
        sin(theta), cos(theta), 0,
        0, 0, 1;
    ExpDelta.block(0,2,2,1) = V*xi.head(2);

    SE2 result = SE2(ExpDelta);

    return result;
  }

  static TangentVector Log(const SE2& T)
  {
    double theta = SO2::Log(T.R())(0);

    double a, b;
    if(theta < 0.0000001)
    {
      a = 1;
      b = 0;
    }
    else
    {
      a = sin(theta)/theta;
      b = (1 - cos(theta))/theta;
    }

    double scale = 1/(a*a + b*b);
    Eigen::MatrixXd V_inv(2,2);
    V_inv <<
        a*scale, b*scale,
        -b*scale, a*scale;

    Eigen::Vector2d temp = V_inv*T.t();
    Eigen::Vector3d xi;
    
    xi << temp(0), temp(1), theta;

    return xi;      
  }

  static Matrix3dRow Ad(const SE2& T)
  {
    Matrix2dRow R = T.R()();
    Eigen::Vector2d t = T.t();    
    
    Matrix3dRow ad_T;
    ad_T.block(0, 0, 2, 2) = R;
    ad_T(0, 2) = t(1);
    ad_T(1, 2) = -t(0);
    ad_T(2, 0) = 0;
    ad_T(2, 1) = 0;
    ad_T(2, 2) = 1;        

    return ad_T;
  }

  using LieGroup<SE2, 3>::inverse;
};

/////////////////////////////////
// Other Useful Operators
/////////////////////////////////
std::ostream& operator<<(std::ostream& os, const SE2& rhs)  
{
  os << rhs();
  return os;
}

}; // namespace Lie
#endif //__LIE_SE3_HPP
