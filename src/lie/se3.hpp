#ifndef __LIE_SE3_HPP
#define __LIE_SE3_HPP

#include <Eigen/Core>
#include <iostream>

#include "so3.hpp"

namespace Lie
{

typedef Eigen::Matrix<double, 4, 4, Eigen::RowMajor> Matrix4dRow;
typedef Eigen::Matrix<double, 6, 6, Eigen::RowMajor> Matrix6dRow;
typedef Eigen::Matrix<double, 6, 1>  Vector6d;

///////////////////////////////////////////////////////////
// The SE3 class represents an element of the SE(3) group
///////////////////////////////////////////////////////////
class SE3 : public LieGroup<SE3, 6>
{
 private:
  Matrix4dRow H_;

 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  SE3()
      : H_()
  {
    H_ = Matrix4dRow::Identity();
  }
  
  SE3(const SE3& H)
      : H_(H())
  {}
  
  SE3(const Matrix4dRow& H)
      : H_(H)
  {}

  SE3(const SO3& R,
      const Eigen::Vector3d& t)
      : H_()
  {
    H_.block(0, 0, 3, 3) = R();
    H_.block(0, 3, 3, 1) = t;

    H_(3, 0) = 0;
    H_(3, 1) = 0;
    H_(3, 2) = 0;
    H_(3, 3) = 1;
  }
  
  SE3(double x,
      double y,
      double z,
      double r,
      double p,
      double h)
      : H_()
  {
    
    SO3 R(r, p, h);
    H_.block(0, 0, 3, 3) = R();
    H_(0, 3) = x;
    H_(1, 3) = y;
    H_(2, 3) = z;

    H_(3, 0) = 0;
    H_(3, 1) = 0;
    H_(3, 2) = 0;
    H_(3, 3) = 1;
  }
  
  ~SE3() {}

  SO3 R() const
  {
    SO3 R(H_.block(0, 0, 3, 3));
    return R;
  }

  Eigen::Vector3d t() const
  {
    Eigen::Vector3d t = H_.block(0, 3, 3, 1);
    return t;
  }
  
  SE3 inverse() const
  {
    SE3 inv(this->R().inverse(),
            this->R().inverse()() * this->t() * (-1));
    return inv;
  }

  // Multiplication Action
  SE3 operator*(const SE3& rhs) const 
  {
    Matrix4dRow result = (*this)()*rhs();
    SE3 H = SE3(result);
    return H;
  }

  Matrix4dRow operator()() const
  {
    return H_;
  }

  static SE3 Exp(const TangentVector& xi)
  {
    Eigen::Vector3d p = xi.head<3>();
    Eigen::Vector3d w = xi.tail<3>();

    Matrix4dRow T = Matrix4dRow::Identity();
    T.block(0,0,3,3) = SO3::Exp(w)();
    T.block(0,3,3,1) = SO3::J_l(w)*p;

    return T;
  }

  static TangentVector Log(const SE3& T)
  {
    Eigen::Vector3d t = T.t();

    Eigen::Vector3d w = SO3::Log(T.R());

    Vector6d xi;
    xi.head<3>() = SO3::J_l_inv(w)*t;
    xi.tail<3>() = w;

    return xi;      
  }

  static Matrix6dRow Ad(const SE3& T)
  {

    Matrix3dRow R = T.R()();
    TangentVector xi = SE3::Log(T);
    Eigen::Vector3d phi = xi.tail<3>();
    //    so3 phi(xi.w());
    Matrix3dRow J = SO3::J_l(phi);
  
    Matrix6dRow ad_T;
    ad_T.block(0, 0, 3, 3) = R;
    ad_T.block(3, 0, 3, 3) = Eigen::Matrix3d::Zero();
    ad_T.block(3, 3, 3, 3) = R;
    ad_T.block(0, 3, 3, 3) = skew(J * xi.head<3>())*R;

    return ad_T;
  }

  using LieGroup<SE3, 6>::inverse;
};

/////////////////////////////////
// Other Useful Operators
/////////////////////////////////
std::ostream& operator<<(std::ostream& os, const SE3& rhs)  
{
  os << rhs();
  return os;
}

}; // namespace Lie
#endif //__LIE_SE3_HPP
