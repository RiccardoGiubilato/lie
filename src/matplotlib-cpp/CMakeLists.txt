file(GLOB implem_files *.c *.cpp *.cc)
file(GLOB header_files *.h *.hpp)

enable_language(CXX)
# set (CMAKE_CXX_FLAGS "-std=c++11 -O3 ${CMAKE_CXX_FLAGS}")

set(LIB_NAME matplotlib-cpp)

add_library(${LIB_NAME} STATIC ${implem_files} ${header_files})

# # link against libraries via pkg-config
set(REQUIRED_PACKAGES python-2.7)
pods_use_pkg_config_packages(${LIB_NAME} ${REQUIRED_PACKAGES})


# install library
pods_install_libraries(${LIB_NAME})
pods_install_headers(${header_files} DESTINATION ${LIB_NAME})
# create a pkg_config file for the library
pods_install_pkg_config_file(${LIB_NAME}
  LIBS -lmatplotlib-cpp
  CFLAGS ${REQUIRED_CFLAGS}  
  REQUIRES ${REQUIRED_PACKAGES} 
  VERSION 0.0.1)

# # Process the test directory
# add_subdirectory(tests)
