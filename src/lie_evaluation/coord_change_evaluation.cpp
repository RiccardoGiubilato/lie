////////////////////////////////
// Generates Figure 7

#include "lie/se3.hpp"
#include "lie/coord_change.hpp"
#include "mr/stat.hpp"


#include "lie_utils.hpp"

Lie::SE3 xyzrph_to_SE3(const Eigen::VectorXd& xyzrph)
{
  Lie::SE3 H(xyzrph(0),
             xyzrph(1),
             xyzrph(2),
             xyzrph(3),
             xyzrph(4),
             xyzrph(5));
  return H;
}

Eigen::VectorXd SE3_to_xyzrph(Lie::SE3& H)
{
  double x, y, z, r, p, h;

  auto t = H.t();
  x = t(0); y = t(1); z = t(2);
  auto R = H.R()();
  h = atan2(R(1,0), R(0,0));
  double sh = sin(h);
  double ch = cos(h);
  p = atan2(-R(2,0), R(0,0)*ch + R(1,0)*sh);
  r = atan2 (R(0,2)*sh - R(1,2)*ch, -R(0,1)*sh + R(1,1)*ch);

  Eigen::VectorXd xyzrph(6);
  xyzrph << x, y, z, r, p, h;

  return xyzrph;
}

int main()
{
  double PI = 3.14159265359;  

  Lie::SE3 T_g1_mu (3, 3, 0, 0.0, 0.0, PI/4.0);//-PI/4.0);

  Eigen::VectorXd diag(6);
  diag << 0.01, 0.01, 0.00001, 0.00001, 0.00001, 0.1;
  Eigen::MatrixXd cov = diag.asDiagonal();

  auto T_g1 = Lie::make_uncertain_state(T_g1_mu, cov);
  auto T_g1_inverse = T_g1.inverse();
  

  Eigen::VectorXd zero = Eigen::VectorXd::Zero(6);

  int N = 100000;
  Eigen::MatrixXd noise = MR::STAT::sample_mvg(zero, cov, N);

  std::vector<Lie::SE3> samples;
  samples.reserve(N);

  Eigen::MatrixXd xyzrph_samples(6,N);

  for(int i=0;i<N;i++)
  {
    auto T_g1_sample = Lie::compose(Lie::SE3::Exp(noise.col(i)), T_g1_mu);

    samples.push_back(T_g1_sample);

    xyzrph_samples.col(i) = SE3_to_xyzrph(T_g1_sample);
  }    

  // Plot Origin
  LieUtils::plot_frame(0,0,0, "r");


  LieUtils::plot_flattened_sigma_error_ellipsoid_5dsphere(T_g1, sqrt(12.5916), "m", {{"linewidth", "5"}, {"alpha", "0.5"}}, "True Distribution");  
  LieUtils::plot_frame(T_g1_mu);

  // Generate SSC Representation
  auto xyzrph_mu = MR::STAT::mvg_mu(xyzrph_samples);
  auto xyzrph_cov = MR::STAT::mvg_cov(xyzrph_samples);

  // Plot Samples
  LieUtils::plot_se3_elements_2d(samples, "k", "Samples");

  
  // Convert via UT
  std::function< Lie::SE3(const Eigen::VectorXd&) > conv_function = xyzrph_to_SE3;
  auto T_g1_converted_tuple =
      Lie::convert_uncert_rep_from_coord_to_lie_algebra<Lie::SE3, 1>(
          xyzrph_mu,
          xyzrph_cov,
          conv_function);

  auto T_g1_converted = std::get<0>(T_g1_converted_tuple);


  
  // Plot Converted Distribution
  LieUtils::plot_flattened_sigma_error_ellipsoid_5dsphere(T_g1_converted, sqrt(12.5916), "g", {{"linewidth", "5"}, {"alpha", "0.5"}}, "SSC Converted to Algebra via UT", 16, 7);
  LieUtils::plot_frame(T_g1_converted.mu());

  // Plot SSC Representation
  LieUtils::plot_xy_sigma_error_ellipse(xyzrph_mu(0),
                                        xyzrph_mu(1),
                                        xyzrph_cov, sqrt(5.9915), "r", "SSC Sample Covariance",{{"linewidth", "5"},{"alpha", "0.5"}});
  
  
 
  plt::xlim(-5, 6);
  plt::ylim(-3, 6);  
  plt::legend("lower left");  
  plt::xlabel("x (meters)");
  plt::ylabel("y (meters)");
  
  plt::show();
}
