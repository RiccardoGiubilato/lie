////////////////////////////////////
// Generates Plot of Uncertain SE3

#include "lie/se3.hpp"
#include "mr/stat.hpp"

#include "lie_utils.hpp"

Eigen::VectorXd SE3_to_xyzrph(Lie::SE3& H)
{
  double x, y, z, r, p, h;

  auto t = H.t();
  x = t(0); y = t(1); z = t(2);
  auto R = H.R()();
  h = atan2(R(1,0), R(0,0));
  double sh = sin(h);
  double ch = cos(h);
  p = atan2(-R(2,0), R(0,0)*ch + R(1,0)*sh);
  r = atan2 (R(0,2)*sh - R(1,2)*ch, -R(0,1)*sh + R(1,1)*ch);

  Eigen::VectorXd xyzrph(6);
  xyzrph << x, y, z, r, p, h;

  return xyzrph;
}

int main()
{
  double PI = 3.14159265359;  
  Lie::SE3 T_ab_mu (2.5, 2.5, 0, 0.0, 0.0, -PI/4.0);//-PI/4.0);

  Eigen::VectorXd zero = Eigen::VectorXd::Zero(6);
  double tran_scale = 10;
  Eigen::VectorXd diag(6);
  diag << 0.001*tran_scale, 0.001*tran_scale, 0.00001, 0.00001, 0.00001, 0.15;
  Eigen::MatrixXd cov = diag.asDiagonal();

  int NUM_SAMPLES = 100000;
  Eigen::MatrixXd xi_samples = MR::STAT::sample_mvg(zero, cov, NUM_SAMPLES);

  Eigen::MatrixXd xyzrph(6,NUM_SAMPLES);
  
  std::vector<Lie::SE3> samples;
  samples.reserve(NUM_SAMPLES);
  for(int i=0;i<NUM_SAMPLES;i++)
  {
    auto pose = Lie::compose(Lie::SE3::Exp(xi_samples.col(i)), T_ab_mu);
    samples.push_back(pose);

    xyzrph.col(i) = SE3_to_xyzrph(pose);
  }

  ///////////////////////////////////////////
  // Create the Figure
  plt::figure_size(700,700);

  // Plot Origin
  LieUtils::plot_frame(0, 0, 0);

  // Plot Samples
  LieUtils::plot_se3_elements_2d(samples, "k", "Samples");

  // Plot Lie Algebra Based Pose
  auto T_ab = Lie::make_uncertain_state(T_ab_mu, cov);
  LieUtils::plot_flattened_sigma_error_ellipsoid_5dsphere(T_ab, sqrt(12.5916), "g", {{"linewidth", "2"}, {"alpha", "0.5"}}, "Lie Algebra 99\% Likely");

  // Plot XY Cov from SSC representation
  Eigen::VectorXd xyzrph_mu = MR::STAT::mvg_mu(xyzrph);
  Eigen::MatrixXd xyzrph_sample_cov = MR::STAT::mvg_cov(xyzrph);
  LieUtils::plot_xy_sigma_error_ellipse(T_ab_mu.t()(0), T_ab_mu.t()(1), xyzrph_sample_cov, sqrt(5.9915), "r", "XY Sample Cov 99\% Likely", {{"linewidth", "2"}, {"alpha", "0.5"}});


  plt::xlim(-2.0, 6.0);
  plt::ylim(-2.0, 6.0);      
  //  plt::axis("equal");

  plt::legend("upper right");

  ///////////////////////////////////////////
  // Generate the Figure Again with Just SSC
  plt::figure_size(700,700);

  // Plot Origin
  LieUtils::plot_frame(0, 0, 0);

  // Plot Samples
  LieUtils::plot_se3_elements_2d(samples, "k", "Samples");

  // Plot XY Cov from SSC representation
  LieUtils::plot_xy_sigma_error_ellipse(T_ab_mu.t()(0), T_ab_mu.t()(1), xyzrph_sample_cov, sqrt(5.9915), "r", "XY Sample Cov 99\% Likely", {{"linewidth", "2"}, {"alpha", "0.5"}});

  plt::xlim(-2.0, 6.0);
  plt::ylim(-2.0, 6.0);      
  //  plt::axis("equal");

  plt::legend("upper right");

  plt::show();
}

