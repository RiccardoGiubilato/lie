/////////////////////////////////////
// Generates Figures 4, 8, 9

#include "lie/se3.hpp"
#include "mr/stat.hpp"
#include "mr/ct.hpp"
#include "mr/misc.hpp"

#include "lie_utils.hpp"

Eigen::MatrixXd threshold_matrix(const Eigen::MatrixXd& mat, double min)
{
  auto new_mat = mat;
  for(int i=0;i<mat.rows();i++)
  {
    for(int j=0;j<mat.cols();j++)
    {
      if(fabs(mat(i, j)) < min )
      {
        new_mat(i, j) = 0;
      }
    }
  }
  return new_mat;
}

Eigen::VectorXd SE3_to_xyzrph(const Lie::SE3& H)
{
  double x, y, z, r, p, h;

  auto t = H.t();
  x = t(0); y = t(1); z = t(2);
  auto R = H.R()();
  h = atan2(R(1,0), R(0,0));
  double sh = sin(h);
  double ch = cos(h);
  p = atan2(-R(2,0), R(0,0)*ch + R(1,0)*sh);
  r = atan2 (R(0,2)*sh - R(1,2)*ch, -R(0,1)*sh + R(1,1)*ch);

  Eigen::VectorXd xyzrph(6);
  xyzrph << x, y, z, r, p, h;

  return xyzrph;
}

std::vector<double>
perform_evaluation(double rho=0.4, double rot_scale=5, double tran_scale=5, double NUM_STEPS=13, bool plot=false)
{

  std::vector<double> result;
  
  // Use Lie Algebra Based Uncertainty Propogation
  double PI = 3.14159265359;  
  Lie::SE3 T_ab_mu (1, 0, 0, 0.0, 0.0, 0);//-PI/4.0);

  Eigen::VectorXd zero = Eigen::VectorXd::Zero(6);
  Eigen::VectorXd diag(6);
  diag << 0.001*tran_scale, 0.00001*tran_scale, 0.00001, 0.00001, 0.00001, 0.003*rot_scale;
  Eigen::MatrixXd cov = diag.asDiagonal();

  Eigen::MatrixXd cross_cov = cov*rho;
  Eigen::MatrixXd block_cov(12,12);
  block_cov.block(0,0,6,6) = cov;
  block_cov.block(6,6,6,6) = cov;  
  block_cov.block(6,0,6,6) = cross_cov;
  block_cov.block(0,6,6,6) = cross_cov;
  
  int NUM_SAMPLES = 10000;

  std::vector<Lie::SE3> final_poses;
 
  Lie::SE3 origin(0,0,0,0,0,0);
  Eigen::VectorXd diag2(6);  
  diag2 << 0, 0, 0, 0, 0, 0;//0.00001, 0.00001, 0.00001, 0.00001, 0.00001, 0.00001;
  Eigen::MatrixXd cov_origin = diag2.asDiagonal();
  auto T_gt = Lie::make_uncertain_state(origin, cov_origin);
  auto T_ab = Lie::make_uncertain_state(T_ab_mu, cov);

  auto T_gt_indep = Lie::make_uncertain_state(origin, cov_origin);

  // Do the same with SSC
  Eigen::VectorXd x_g(6); x_g << 0, 0, 0, 0, 0, 0;
  Eigen::VectorXd x_ab_mean(6); x_ab_mean << 1, 0, 0, 0, 0, 0;
  Eigen::VectorXd x_gt = x_g;
  Eigen::MatrixXd Sigma_x_gt = Eigen::MatrixXd::Zero(6,6);

  // Sample Covariance Data
  Eigen::MatrixXd final_locations(2, NUM_SAMPLES);

  std::vector< std::pair< std::vector<double>,
                          std::vector<double> > > tracks;
                          

  for(int i=0;i<NUM_SAMPLES;i++)
  {
    Eigen::MatrixXd noise_z = MR::STAT::sample_mvg(zero, cov, NUM_STEPS);
    
    Lie::SE3 T_gt_sample(0, 0, 0, 0, 0, 0);

    std::vector<double> track_x; track_x.push_back(0);
    std::vector<double> track_y; track_y.push_back(0);

    Eigen::VectorXd noise_last(6);
    
    for(int t=0;t<NUM_STEPS;t++)
    {
      
      Eigen::VectorXd noise(6);

      if(t == 0)
      {
        noise = noise_z.col(0);
      }
      else
      {
        //https://stats.stackexchange.com/questions/13382/how-to-define-a-distribution-such-that-draws-from-it-correlate-with-a-draw-from/13384#13384
        noise = rho*noise_last + sqrt(1 - rho*rho)*noise_z.col(t);
      }

      auto T_ab_noisy = Lie::compose(Lie::SE3::Exp(noise), T_ab_mu);
      
      T_gt_sample = Lie::compose(T_gt_sample, T_ab_noisy);//std::get<0>(refs), std::get<1>(refs));

      track_x.push_back(T_gt_sample.t()(0));
      track_y.push_back(T_gt_sample.t()(1));

      // Track uncertainty using proposed methods
      if(i==0)
      {
        Eigen::MatrixXd local_block_cov = block_cov;
        local_block_cov.block(0,0,6,6) = T_gt.marginal_cov();
        
        auto refs = Lie::make_uncertain_state(
            std::make_tuple(T_gt.mu(), T_ab.mu()),
            local_block_cov);

        T_gt = Lie::compose(std::get<0>(refs), std::get<1>(refs));

        T_gt_indep = Lie::compose(T_gt_indep, T_ab);

      }

      // Track uncertainty using SSC
      if(i==0)
      {
        Eigen::MatrixXd Sigma_x_gtm1_x_ab = Eigen::MatrixXd::Zero(12,12);
        Sigma_x_gtm1_x_ab.block(0,0,6,6) = Sigma_x_gt;
        Sigma_x_gtm1_x_ab.block(6,6,6,6) = cov;
        Sigma_x_gtm1_x_ab.block(0,6,6,6) = cross_cov;
        Sigma_x_gtm1_x_ab.block(6,0,6,6) = cross_cov;                        

        Eigen::MatrixXd J = MR::SSC::head2tail_jac(x_gt, x_ab_mean);
        x_gt = MR::SSC::head2tail(x_gt, x_ab_mean);
        
        Sigma_x_gt = J * Sigma_x_gtm1_x_ab * J.transpose();
      }

      noise_last = noise;
    }

    final_poses.push_back(T_gt_sample);
    final_locations.col(i) << T_gt_sample.t()(0), T_gt_sample.t()(1);

    tracks.push_back( std::make_pair(track_x, track_y) );
  }

  if(plot)
  {
    for( auto track_pair : tracks )
    {
      plt::plot(track_pair.first, track_pair.second, "0.2");
    }
  }
  
  // Plot Sample Covariance Matrix
  auto Mean_Final_Position = MR::STAT::mvg_mu(final_locations);
  auto Sample_Cov = MR::STAT::mvg_cov(final_locations);  

  if(plot)
  {
  
    LieUtils::plot_xy_sigma_error_ellipse(x_gt(0), x_gt(1), Sigma_x_gt, sqrt(5.9915), "r", "SSC Head-to-Tail ([5])", {{"linewidth", "2"}, {"alpha", "0.5"}});

  
    //    LieUtils::plot_flattened_sigma_error_ellipsoid_5dsphere(T_gt, sqrt(9.2103), "g", {{"linewidth", "5"}, {"alpha", "0.5"}}, "Joint Lie Algebra Comp (Proposed)", 18);  
  
    //    LieUtils::plot_flattened_sigma_error_ellipsoid_5dsphere(T_gt_indep, sqrt(9.2103), "c", {{"linewidth", "5"}, {"alpha", "0.5"}}, "Indep. Lie Algebra Comp ([1])", 18);

    LieUtils::plot_flattened_sigma_error_ellipsoid_5dsphere(T_gt, sqrt(12.56), "g", {{"linewidth", "4"}, {"alpha", "0.5"}}, "Joint Lie Algebra Comp (Proposed)", 20, 10);  
  
    LieUtils::plot_flattened_sigma_error_ellipsoid_5dsphere(T_gt_indep, sqrt(12.56), "c", {{"linewidth", "4"}, {"alpha", "0.5"}}, "Indep. Lie Algebra Comp ([1])", 20, 10);

    
    plt::legend("lower left");
    plt::xlabel("x (meters)");
    plt::ylabel("y (meters)");
    plt::ylim(-12,10);//axis("equal");  
    plt::xlim(-7, 18);
  }

  // Including Cross Covariance
  std::cout << "Joint Propogation in the Lie Algebra: \n";
  auto Information = threshold_matrix(T_gt.marginal_cov().inverse(), 1e-8);
  
  double num_valid_samples_99p9perc = 0;
  double num_valid_samples_68p27perc = 0;
  for(auto s : final_poses)
  {
    auto error = Lie::compose(s, T_gt.mu().inverse());    
    auto error_vec = Lie::SE3::Log(error);
    auto mahal_dist_squared = error_vec.transpose() * Information * error_vec;

    if(mahal_dist_squared(0) < 22.4577) //p = 0.99p973, dof=6 20.061901972375509
    {
      num_valid_samples_99p9perc++;
    }
  }


  double perc_valid_99p9 = num_valid_samples_99p9perc/NUM_SAMPLES*100.0;
  std::cout << "Percent within 99.9% covariance bounds (with correlation): " << perc_valid_99p9 << "\n";
  result.push_back(perc_valid_99p9);
  
  // Independent - Excluding Cross Covariance
  std::cout << "Independent Propogation in the Lie Algebra: \n";  
  auto Information_indep = threshold_matrix(T_gt_indep.marginal_cov().inverse(), 1e-8);
  
  num_valid_samples_99p9perc = 0;
  num_valid_samples_68p27perc = 0;
  for(auto s : final_poses)
  {
    auto error = Lie::compose(s, T_gt_indep.mu().inverse());    
    auto error_vec = Lie::SE3::Log(error);
    auto mahal_dist_squared = error_vec.transpose() * Information_indep * error_vec;

    //    std::cout << error_vec.transpose() << "\n";
    //    std::cout << mahal_dist_squared << "\n";    

    if(mahal_dist_squared(0) < 22.4577) //p = 0.99p973, dof=6 20.061901972375509
    {
      num_valid_samples_99p9perc++;
    }
  }

  
  

  perc_valid_99p9 = num_valid_samples_99p9perc/NUM_SAMPLES*100.0;
  std::cout << "Percent within 99.9% covariance bounds (w/o correlation): " << perc_valid_99p9 << "\n";

  result.push_back(perc_valid_99p9);

  // SSC Head To Tail
  std::cout << "SSC Head To Tail: \n";
  auto Information_ssc = threshold_matrix(Sigma_x_gt.inverse(), 1e-8);
  
  num_valid_samples_99p9perc = 0;
  num_valid_samples_68p27perc = 0;
  for(auto s : final_poses)
  {
    Eigen::VectorXd error = SE3_to_xyzrph(T_gt.mu()) - SE3_to_xyzrph(s);
    Eigen::VectorXd error_vec = error;
    error_vec(3) = minimized_angle(error(3));
    error_vec(4) = minimized_angle(error(4));
    error_vec(5) = minimized_angle(error(5));    
    
    auto mahal_dist_squared = error_vec.transpose() * Information_ssc * error_vec;

    if(mahal_dist_squared(0) < 22.4577)//20.061901972375509) //p = 0.99p973, dof=6 20.061901972375509
    {
      num_valid_samples_99p9perc++;
    }
  }

  
  

  perc_valid_99p9 = num_valid_samples_99p9perc/NUM_SAMPLES*100.0;
  std::cout << "Percent within 99.9% covariance bounds (SSC): " << perc_valid_99p9 << "\n";

  result.push_back(perc_valid_99p9);

  return result;
}



int main()
{
  plt::rc("text", {{"usetex", "True"}});
  plt::rc("font", {{"size", "16"}});      
  plt::rc("legend", {{"fontsize", "14"}});  
  
  
  // Plot Example Plot
  plt::figure();
  perform_evaluation(0.4, 5, 5, 10, true);

  //  plt::show();
  
  // Evaluate vs. Num Steps
  {
    std::vector<double> num_steps;
    for(int i=1;i<50;i=i+1) num_steps.push_back(i);

    std::vector<double> percent_inside_joint;
    std::vector<double> percent_inside_indep;
    std::vector<double> percent_inside_ssc;  
  
    for( auto steps : num_steps)
    {
      auto result = perform_evaluation(0.4, 3, 3, steps, false);
      percent_inside_joint.push_back(result[0]);
      percent_inside_indep.push_back(result[1]);
      percent_inside_ssc.push_back(result[2]);
    }

    plt::figure();
    plt::xlabel("Num Steps ($N$)");
    plt::ylabel("Percent Likely Samples");
    plt::ylim(0, 100);

    plt::named_plot(std::string("Joint Lie Algebra Comp (Proposed)"), num_steps, percent_inside_joint, "g");
    plt::named_plot("Indep. Lie Algebra Comp ([1])", num_steps, percent_inside_indep, "c");
    plt::named_plot("SSC Head-to-tail ([5])", num_steps, percent_inside_ssc, "r");
    plt::legend("upper right");
  }
  
  // Evaluate vs. Noise Scale
  {
    std::vector<double> noise_scale;
    for(int i=1;i<25;i=i+1) noise_scale.push_back(i);

    std::vector<double> percent_inside_joint;
    std::vector<double> percent_inside_indep;
    std::vector<double> percent_inside_ssc;  
  
    for( auto scale : noise_scale)
    {
      auto result = perform_evaluation(0.4, scale, 3, 10, false);
      percent_inside_joint.push_back(result[0]);
      percent_inside_indep.push_back(result[1]);
      percent_inside_ssc.push_back(result[2]);
    }

    plt::figure();
    plt::subplot(2,1,1);
    plt::xlabel("Rotation Noise Scale ($\\sigma_r$)");
    plt::ylabel("Percent Likely Samples");
    plt::ylim(0, 100);

    plt::named_plot(std::string("Joint Lie Algebra Comp (Proposed)"), noise_scale, percent_inside_joint, "g");
    plt::named_plot("Indep. Lie Algebra Comp ([1])", noise_scale, percent_inside_indep, "c");
    plt::named_plot("SSC Head-to-tail ([5])", noise_scale, percent_inside_ssc, "r");
  }
  {
    std::vector<double> noise_scale;
    for(int i=1;i<25;i=i+1) noise_scale.push_back(i);

    std::vector<double> percent_inside_joint;
    std::vector<double> percent_inside_indep;
    std::vector<double> percent_inside_ssc;  
  
    for( auto scale : noise_scale)
    {
      auto result = perform_evaluation(0.4, 3, scale, 10, false);
      percent_inside_joint.push_back(result[0]);
      percent_inside_indep.push_back(result[1]);
      percent_inside_ssc.push_back(result[2]);
    }

    plt::subplot(2,1,2);
    plt::xlabel("Translation Noise Scale ($\\sigma_t$)");
    plt::ylabel("Percent Likely Samples");
    plt::ylim(0, 100);

    plt::named_plot(std::string("Joint Lie Algebra Comp (Proposed)"), noise_scale, percent_inside_joint, "g");
    plt::named_plot("Indep. Lie Algebra Comp ([1])", noise_scale, percent_inside_indep, "c");
    plt::named_plot("SSC Head-to-tail ([5])", noise_scale, percent_inside_ssc, "r");
    plt::legend("lower left");

    plt::tight_layout();
  }
  
  plt::show();
}

 
