#include "lie/se3.hpp"
#include "mr/stat.hpp"
#include "mr/ct.hpp"

#include "lie_utils.hpp"

Eigen::MatrixXd threshold_matrix(const Eigen::MatrixXd& mat, double min)
{
  auto new_mat = mat;
  for(int i=0;i<mat.rows();i++)
  {
    for(int j=0;j<mat.cols();j++)
    {
      if(fabs(mat(i, j)) < min )
        //         fabs(mat(i, j)) > max)
      {
        new_mat(i, j) = 0;
      }
    }
  }
  return new_mat;
}

int main()
{
  // Use Lie Algebra Based Uncertainty Propogation
  double PI = 3.14159265359;  
  Lie::SE3 T_ab_mu (1, 0, 0, 0.0, 0.0, 0);//-PI/4.0);

  Eigen::VectorXd zero = Eigen::VectorXd::Zero(6);
  Eigen::VectorXd diag(6);
  diag << 0.00001, 0.00001, 0.00001, 0.00001, 0.00001, 0.0009;
  Eigen::MatrixXd cov = diag.asDiagonal();


  int NUM_SAMPLES = 1000;
  int NUM_STEPS = 100;  

  std::vector<Lie::SE3> final_poses;
 
  Lie::SE3 origin(0,0,0,0,0,0);
  Eigen::VectorXd diag2(6);  
  diag2 << 0, 0, 0, 0, 0, 0;//0.00001, 0.00001, 0.00001, 0.00001, 0.00001, 0.00001;
  Eigen::MatrixXd cov_origin = diag2.asDiagonal();
  auto T_gt = Lie::make_uncertain_state(origin, cov_origin);
  auto T_ab = Lie::make_uncertain_state(T_ab_mu, cov);

  // Do the same with SSC
  Eigen::VectorXd x_g(6); x_g << 0, 0, 0, 0, 0, 0;
  Eigen::VectorXd x_ab_mean(6); x_ab_mean << 1, 0, 0, 0, 0, 0;
  Eigen::VectorXd x_gt = x_g;
  Eigen::MatrixXd Sigma_x_gt = Eigen::MatrixXd::Zero(6,6);

  // Sample Covariance Data
  Eigen::MatrixXd final_locations(2, NUM_SAMPLES);

  for(int i=0;i<NUM_SAMPLES;i++)
  {
    Eigen::MatrixXd noise = MR::STAT::sample_mvg(zero, cov, NUM_STEPS);
    
    Lie::SE3 T_gt_sample(0, 0, 0, 0, 0, 0);

    std::vector<double> track_x; track_x.push_back(0);
    std::vector<double> track_y; track_y.push_back(0);
    
    for(int t=0;t<NUM_STEPS;t++)
    {
      auto T_ab_noisy = Lie::compose(Lie::SE3::Exp(noise.col(t)), T_ab_mu);

      T_gt_sample = Lie::compose(T_gt_sample, T_ab_noisy);

      track_x.push_back(T_gt_sample.t()(0));
      track_y.push_back(T_gt_sample.t()(1));

      // Track uncertainty using proposed methods
      if(i==0)
      {
        T_gt = Lie::compose(T_gt, T_ab);
      }

      // Track uncertainty using SSC
      if(i==0)
      {
        Eigen::MatrixXd Sigma_x_gtm1_x_ab = Eigen::MatrixXd::Zero(12,12);
        Sigma_x_gtm1_x_ab.block(0,0,6,6) = Sigma_x_gt;
        Sigma_x_gtm1_x_ab.block(6,6,6,6) = cov;        

        Eigen::MatrixXd J = MR::SSC::head2tail_jac(x_gt, x_ab_mean);
        x_gt = MR::SSC::head2tail(x_gt, x_ab_mean);
        
        Sigma_x_gt = J * Sigma_x_gtm1_x_ab * J.transpose();
      }
      
    }

    final_poses.push_back(T_gt_sample);
    final_locations.col(i) << T_gt_sample.t()(0), T_gt_sample.t()(1);

    plt::plot(track_x, track_y, "grey");
  }

  //  LieUtils::plot_se3_elements_2d(final_poses, "b", true);



  // Plot SSC Estimated Covariance
  //  LieUtils::plot_frame(x_gt(0), x_gt(1), x_gt(5), "r");
  //  std::cout << "Sigma_x_gt:\n" << Sigma_x_gt << "\n";
  LieUtils::plot_xy_sigma_error_ellipse(x_gt(0), x_gt(1), Sigma_x_gt, 3, "r", "SSC Head-to-Tail");

  // Plot Lie Algebra Based Uncertainty Estimate
  LieUtils::plot_flattened_sigma_error_ellipsoid_3dsphere(T_gt, sqrt(20.06), "g", "Lie Algebra Pose Composition");

  // Plot Sample Covariance Matrix
  auto Mean_Final_Position = MR::STAT::mvg_mu(final_locations);
  auto Sample_Cov = MR::STAT::mvg_cov(final_locations);  
  LieUtils::plot_xy_sigma_error_ellipse(Mean_Final_Position(0),
                                        Mean_Final_Position(1),
                                        Sample_Cov, 3, "b", "Sample Covariance");

  
  plt::rc("legend", {{"fontsize", "16"}});  
  plt::legend("lower left");
  plt::xlabel("x (meters)");
  plt::ylabel("y (meters)");
  plt::axis("equal");
  
  plt::xlim(0, 100);

  //  plt::scatter(std::vector<double>({0}), std::vector<double>({0}), 3, "b");


  std::cout << "Pose T_ab mu:\n" << T_ab_mu << "\n";
  std::cout << "Xi_ab cov:\n" << cov << "\n";


  // Percent Samples that fall within lie algebra ellipsoid
  std::cout << T_gt.marginal_cov() << "\n";  
  std::cout << T_gt.marginal_cov().inverse() << "\n";  
  auto Information = threshold_matrix(T_gt.marginal_cov().inverse(), 1e-8);
  std::cout << Information << "\n";
  
  double num_valid_samples_99p73perc = 0;
  double num_valid_samples_68p27perc = 0;
  for(auto s : final_poses)
  {
    auto error = Lie::compose(s, T_gt.mu().inverse());    
    auto error_vec = Lie::SE3::Log(error);
    std::cout << "\n" << error_vec.transpose() << "\n";
    auto mahal_dist_squared = error_vec.transpose() * Information * error_vec;

    //    std::cout << mahal_dist_squared << "\n";
    if(mahal_dist_squared(0) < 20.061901972375509) //p = 0.9973, dof=6 20.061901972375509

    {
      num_valid_samples_99p73perc++;
    }
    if(mahal_dist_squared(0) < 7.038515491144959) //p = 0.6827, dof=6 7.038515491144959
    {
      num_valid_samples_68p27perc++;
    }    
  }


  double perc_valid_99p73 = num_valid_samples_99p73perc/NUM_SAMPLES*100.0;
  std::cout << "Percent within 99.73% (3-Sigma) covariance bounds (lie-algebra): " << perc_valid_99p73 << "\n";
  double perc_valid_68p27 = num_valid_samples_68p27perc/NUM_SAMPLES*100.0;
  std::cout << "Percent within 68.27% (1-Sigma) covariance bounds (lie-algebra): " << perc_valid_68p27 << "\n";  
  
  
  plt::show();
}
 
