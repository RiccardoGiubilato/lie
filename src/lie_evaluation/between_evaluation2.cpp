///////////////////////////////////
// Generates Figure 6

#include "lie/se3.hpp"
#include "mr/stat.hpp"

#include "lie_utils.hpp"

Eigen::MatrixXd threshold_matrix(const Eigen::MatrixXd& mat, double min)
{
  auto new_mat = mat;
  for(int i=0;i<mat.rows();i++)
  {
    for(int j=0;j<mat.cols();j++)
    {
      if(fabs(mat(i, j)) < min )
      {
        new_mat(i, j) = 0;
      }
    }
  }
  return new_mat;
}

double covariance_error(const Eigen::MatrixXd& Sigma_MC, const Eigen::MatrixXd& Sigma_test)
{
  return sqrt( ( (Sigma_test - Sigma_MC).transpose() * (Sigma_test - Sigma_MC) ).trace() );
}
   

int main()
{
  double PI = 3.14159265359;

  double alpha = 0.0;
  double alpha_max = 2.0;
  std::vector<double> alpha_value;  
  std::vector<double> covariance_error_with_corr;
  std::vector<double> covariance_error_wo_corr;  
  double step = 0.05;
  
  while(alpha <= alpha_max)
  {
    alpha_value.push_back(alpha);

    Lie::SE3 T_g1_mu (3, 3, 0, 0.0, 0.0, PI/4.0);//-PI/4.0);
    Lie::SE3 T_g2_mu (4.5, 4.5, 0, 0.0, 0.0, PI/4.0);//-PI/4.0);    

    Eigen::VectorXd diag(6);
    diag << 0.005, 0.005, 0.00001, 0.00001, 0.00001, 0.006;
    Eigen::MatrixXd cov(12,12);
    cov.block(0,0,6,6) = diag.asDiagonal();
    cov.block(6,6,6,6) = diag.asDiagonal();
    Eigen::VectorXd cross(6);
    cross << 0.0005, 0.0005, 0, 0, 0, 0.005;      
    cov.block(0,6,6,6) = cross.asDiagonal();
    cov.block(6,0,6,6) = cross.asDiagonal();

    cov = cov*alpha;


    auto refs = Lie::make_uncertain_state(std::make_tuple(T_g1_mu, T_g2_mu), cov);
    auto T_g1 = std::get<0>(refs);
    auto T_g2 = std::get<1>(refs);  

    Eigen::VectorXd zero = Eigen::VectorXd::Zero(12);

    int N = 10000;
    Eigen::MatrixXd noise = MR::STAT::sample_mvg(zero, cov, N);

    std::vector<Lie::SE3> samples_g1;
    samples_g1.reserve(N);
    std::vector<Lie::SE3> samples_g2;
    samples_g2.reserve(N);

    std::vector<Lie::SE3> samples_12;
    samples_12.reserve(N);  

    for(int i=0;i<N;i++)
    {
      auto T_g1_sample = Lie::compose(Lie::SE3::Exp(noise.col(i).head<6>()), T_g1_mu);
      auto T_g2_sample = Lie::compose(Lie::SE3::Exp(noise.col(i).tail<6>()), T_g2_mu);    

      samples_g1.push_back(T_g1_sample);
      samples_g2.push_back(T_g2_sample);    

      auto T_12_sample = Lie::between(T_g1_sample, T_g2_sample);
      samples_12.push_back(T_12_sample);
    }

    auto T_g1_indep = Lie::make_uncertain_state(T_g1.mu(), T_g1.marginal_cov());
    auto T_g2_indep = Lie::make_uncertain_state(T_g2.mu(), T_g2.marginal_cov());  
  
    auto T_12_indep = Lie::between(T_g1_indep, T_g2_indep);
    auto T_12 = Lie::between(T_g1, T_g2);

    Eigen::MatrixXd all_samples(6, N);
    int i=0;
    for(auto s : samples_12)
    {
      auto error = Lie::compose(s, T_12.mu().inverse());
      auto error_vec = Lie::SE3::Log(error);
      all_samples.col(i) = error_vec;
      i++;
    }

    Eigen::MatrixXd Sigma_MC = MR::STAT::mvg_cov(all_samples);
    
    double error_with_corr = covariance_error(Sigma_MC, T_12.marginal_cov());
    double error_wo_corr = covariance_error(Sigma_MC, T_12_indep.marginal_cov());  

    covariance_error_with_corr.push_back(error_with_corr);
    covariance_error_wo_corr.push_back(error_wo_corr);    
    alpha += step;
  }

  plt::rc("legend", {{"fontsize", "14"}});  
  plt::named_plot("Indep Rel. Pose", alpha_value, covariance_error_wo_corr, "c", {{"linewidth", "2"}});
  plt::named_plot("Joint Rel. Pose", alpha_value, covariance_error_with_corr, "g", {{"linewidth", "2"}});

  plt::rc("text", {{"usetex", "True"}});
  plt::rc("font", {{"size", "18"}});  
  plt::legend("upper left");
  plt::xlabel("Noise Scaling ($\\alpha$)");
  plt::ylabel("Covariance Error ($\\epsilon$)");
  plt::ylim(-0.005, 0.3);
  
  plt::show();

  return 0;
}
